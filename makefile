# -*- mode: makefile-gmake -*-

TARGETS = \
sod_tube.bin\
viscous_shock.bin\
isentropic_vortex.bin \
test1d.bin

ALL : $(TARGETS)

#isentropic_vortex.bin: | isentropic_vortex.c navierstokes3d.f90
#viscous_shock.bin: | viscous_shock.c navierstokes3d.f90
#tgv.bin: | tgv.c navierstokes3d.f90
#square.bin: | square.c navierstokes3d.f90
#sod.bin: sod.c euler.h

OPTS=-malloc_debug -malloc_dump -nox

clean::
	-@$(RM) $(TARGETS) *.o *.mod
	-@$(RM) -r *.pvd *.pvd.step *.pvtu *.pvtu.part *.vtu

include $(SSDC_DIR)/lib/ssdc/conf/variables
include $(SSDC_DIR)/lib/ssdc/conf/rules

#%.o : %.c
#	$(PETSC_COMPILE_SINGLE) $(abspath $<)


sod_tube.bin :  sod_tube.o    navierstokes1d_v2.o 
	$(CLINKER) -o $@ $^ $(SSDC_LIB)
	$(RM) $^ navierstokes1d_v2.mod

viscous_shock.bin :  viscous_shock.o    navierstokes1d.o 
	$(CLINKER) -o $@ $^ $(SSDC_LIB)
	$(RM) $^ navierstokes1d.mod

test1d.bin :  test1d.o    navierstokes1d_v2.o 
	$(CLINKER) -o $@ $^ $(SSDC_LIB)
	$(RM) $^ navierstokes1d_v2.mod

isentropic_vortex.bin : isentropic_vortex.o navierstokes3d_v2.o 
	$(CLINKER) -o $@ $^ $(SSDC_LIB)
	$(RM) $^ navierstokes3d_v2.mod
%.bin : %.o
	$(CLINKER) -o $@ $< $(SSDC_LIB)
	@$(RM) -f $<
