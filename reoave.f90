
module globals
  implicit none
  !
  !   This is the number of points used to discretize X
  integer, parameter :: nx   = 48
  !   Here we set the extent of X and calculate $\Delta x$
  real, parameter    :: xmax = 1.0
  real, parameter    :: dx   = xmax/float(nx)
  !   This is the speed of propagation of the advection equation
  !   (a in the notes)
  real, parameter     :: speed = 1.5
  real, parameter     :: gama  = 1.4
  !
  !   This is a vector that contains u(x)
 ! real :: u(0:nx+1)
   integer, parameter :: dim = 1
   integer, parameter :: neq = dim + 2 
   real               :: u(neq,0:nx+1)
   real               :: v(neq,0:nx+1)
  !  
end module globals

program tests

  use globals
  implicit none
  ! declaration of some variables needed by the main program
  real            :: time, dt , vl(neq), vr(neq), vroe1(neq), vroe2(neq), vroe3(neq)            !  t, $\Delta t$
  real, parameter :: tmax= 0.2             ! maximumn integration time
  real, parameter :: dtprint= 0.01          ! interval between outputs
  real            :: tprint               ! time of next output
  integer         :: itprint              ! number of current output

vl(1) = 1.0
vr(1) = 0.125

vl(2) = 0.0
vr(2) = 0.0

vl(3) = 1.1
vr(3) = 0.1
  ! This subroutine generates the initial conditions
call roe_ave2000(vl,vr,vroe2)
call roe_ave1980(vl,vr,vroe1)
call roeavg2000( vl,vr,vroe3)

print*,"this is roe 2000"
print*, vroe2


print*,"this is roe 1980"
print*, vroe1

print*,"this is roe 2000-2"
print*, vroe3

end program tests


  subroutine roe_ave2000(vl,vr,vhat)
  use globals, only: gama,neq
    implicit none

    ! Arguments
    ! =========
    real, intent(in), dimension(neq)     :: vl, vr   ! left and right states

    real, intent(out), dimension(neq) :: vhat             ! temporary variables


    real :: root_l, root_r          ! 1/sqrt[T_i], i=L,R

    real :: tinvav, tinvavinv              ! average of inverse temperature and its inverse

    real :: z3_ave, z1_ave                        ! Logarithmic averages of density and temperature

    real :: mdot, P1, P2  ,gm1, gamma0 ,gm1og,gp1og              ! normal mass flux (mdot), Pressure,  Temperature

   gm1    = gama-1.0
   gamma0 = gama
   gm1og  = (gama - 1.0)/gamma0
   gp1og  = (gama + 1.0)/gamma0
    continue

    root_l   = sqrt(vl(1)/vl(3))    ; root_r   = sqrt(vr(1)/vr(3))            ! Sqrt[T_i]
        ! Sqrt[T_i]^{-1}

    tinvav    = root_l  + root_r
    tinvavinv = 1.0/tinvav

    vhat(2) = (vl(2)*root_l + vr(2)*root_r)*tinvavinv   ! velocity

    P1 = (vl(3)*root_l + vr(3)*root_r) * tinvavinv                 ! pressure, p_1 in (Ismail and Roe)

    call Logarithmic_Average(root_l*vl(3),root_r*vr(3),z3_ave )  !z3 log ave          ! Logarithmic average of rho/Sqrt(T)

    call Logarithmic_Average(root_l    ,root_r  ,z1_ave  )    !z1 log ave       ! Logarithmic average of   1/Sqrt(T)

    vhat(1) = 0.5*tinvav * z3_ave                                   ! density

    P2 = 0.5*(gm1og * P1 + gp1og * z3_ave/z1_ave)   ! Now it is P_2              ! temperature

    vhat(3) = gamma0*P2/(gm1*vhat(1))  + 0.5*( vhat(2)*vhat(2) ) ! total enthalpy
    vhat(2) = P1

  end  subroutine  roe_ave2000


  subroutine roe_ave1980(vinl,vinr,av)
  use globals, only: gama,neq
  implicit none
  real, intent(in)    :: vinl(neq), vinr(neq)
  real, intent(out)   :: av(neq)
  real                :: invtemp,hl,hr

  invtemp = 1.0/( sqrt(vinl(1)) + sqrt(vinr(1)) )

  hl      = vinl(3)/( vinl(1)*(gama-1.0) ) + vinl(3)/vinl(1) + 0.5*vinl(2)*vinl(2)
  hr      = vinr(3)/( vinr(1)*(gama-1.0) ) + vinr(3)/vinr(1) + 0.5*vinr(2)*vinr(2)

  av(1) = ( sqrt(vinl(1))*vinl(2) + sqrt(vinr(1))*vinr(2) )*invtemp    !averaged velocituy
  av(2) = ( sqrt(vinl(1))*hl + sqrt(vinr(1))*hr )*invtemp     !averaged enthalpy
  av(3) = sqrt(( gama-1.0 )*( av(2)-0.5*av(1)*av(1) ))  
   ! averaged sound speed
  !print*, av(1),av(2),av(3) ,neq
  return
end subroutine roe_ave1980

 subroutine Logarithmic_Average(a,b,loga)

!   use variables, only: Log_Ave_Counter

    implicit none

    real,  intent(in) :: a,b
      real,  intent(out) :: loga

!  Logarithmic expansion valid to us = 0.00000894, or ratio =  1.006
!   real(wp), dimension(0:1), parameter :: c = (/1.0_wp,3.0_wp/5.0_wp/)
!   real(wp), dimension(0:1), parameter :: d = (/2.0_wp,8.0_wp/15.0_wp/)
!   real(wp),                 parameter :: eps = 8.9e-06_wp

!  Logarithmic expansion valid to us = 0.00276, or ratio =  1.111
!   real(wp), dimension(0:2), parameter :: c = (/1.0_wp,10.0_wp/9.0_wp,5.0_wp/21.0_wp /)
!   real(wp), dimension(0:2), parameter :: d = (/2.0_wp,14.0_wp/9.0_wp,128.0_wp/945.0_wp /)
!   real(wp),                 parameter :: eps = 2.7e-03_wp

!  Logarithmic expansion valid to us = 0.0226, or ratio =  1.353
!   real(wp), dimension(0:3), parameter :: c = (/1.0_wp,21.0_wp/13.0_wp,105.0_wp/143.0_wp,35.0_wp/429.0_wp /)
!   real(wp), dimension(0:3), parameter :: d = (/2.0_wp,100.0_wp/39.0_wp,566.0_wp/715.0_wp,512.0_wp/15015.0_wp /)
!   real(wp),                 parameter :: eps = 2.0e-02_wp

!  Logarithmic expansion valid to us = 0.0677, or ratio =  1.703
!   real(wp), dimension(0:4), parameter :: c = (/1.0_wp,36.0_wp/17.0_wp,126.0_wp/85.0_wp, &
!                                                84.0_wp/221.0_wp,63.0_wp/2431.0_wp/)
!   real(wp), dimension(0:4), parameter :: d = (/2.0_wp,182.0_wp/51.0_wp,166.0_wp/85.0_wp, &
!                                              2578.0_wp/7735.0_wp,32768.0_wp/3828825.0_wp/)
!   real(wp),                 parameter :: eps = 6.0e-02_wp

!  Logarithmic expansion valid to us = 0.135, or ratio =  2.165
    real, dimension(0:5), parameter :: c = (/1.0, 55.0/21.0, 330.0/133.0, &
                                        330.0/323.0, 55.0/323.0, 33.0/4199.0/)
    real, dimension(0:5), parameter :: d = (/2.0, 32.0/7.0, 3092.0/855.0, &
                        7808.0/6783.0, 17926.0/142443.0, 131072.0/61108047.0/)
    real,                 parameter :: eps = 1.1e-01

!  Logarithmic expansion valid to us = 0.221, or ratio =  2.776
!   real(wp), dimension(0:6), parameter :: c = (/1.0_wp,78.0_wp/25.0_wp,429.0_wp/115.0_wp,  &
!                                                1716.0_wp/805.0_wp, 1287.0_wp/2185.0_wp,   &
!                                                2574.0_wp/37145.0_wp, 429.0_wp/185725.0_wp/)
!   real(wp), dimension(0:6), parameter :: d = (/2.0_wp,418.0_wp/75.0_wp,3324.0_wp/575.0_wp,  &
!                                               55116.0_wp/20125.0_wp,399118.0_wp/688275.0_wp,&
!                                               1898954.0_wp/42902475.0_wp,                   &
!                                               2097152.0_wp/3904125225.0_wp/)
!   real(wp),                 parameter :: eps = 2.0e-01_wp

!  Logarithmic expansion valid to us = 0.315, or ratio =  3.560
!   real(wp), dimension(0:7), parameter :: c = (/1.0_wp,105.0_wp/29.0_wp,455.0_wp/87.0_wp,  &
!                                                1001.0_wp/261.0_wp,1001.0_wp/667.0_wp,    &
!                                                1001.0_wp/3335.0_wp,1001.0_wp/38019.0_wp, &
!                                                143.0_wp/215441.0_wp/)
!   real(wp), dimension(0:7), parameter :: d = (/2.0_wp,572.0_wp/87.0_wp,3674.0_wp/435.0_wp,  &
!                                                3256.0_wp/609.0_wp, 31054.0_wp/18009.0_wp,   &
!                                        86644.0_wp/330165.0_wp, 3622802.0_wp/244652265.0_wp, &
!                                                8388608.0_wp/62386327575.0_wp /)
!   real(wp),                 parameter :: eps = 3.0e-01_wp



    real              :: xi, gs, us, ave

    real              :: Logarithmic_Averag

    xi  = a/b
    gs  = (xi-1.0)/(xi+1.0)
    us  = gs*gs
    ave = a + b

!   if(                        (us <= 1.0e-8_wp)) Log_Ave_Counter( 1) = Log_Ave_Counter( 1) + 1
!   if((1.0e-8_wp <= us) .and. (us <= 1.0e-7_wp)) Log_Ave_Counter( 2) = Log_Ave_Counter( 2) + 1
!   if((1.0e-7_wp <= us) .and. (us <= 1.0e-6_wp)) Log_Ave_Counter( 3) = Log_Ave_Counter( 3) + 1
!   if((1.0e-6_wp <= us) .and. (us <= 1.0e-5_wp)) Log_Ave_Counter( 4) = Log_Ave_Counter( 4) + 1
!   if((1.0e-5_wp <= us) .and. (us <= 1.0e-4_wp)) Log_Ave_Counter( 5) = Log_Ave_Counter( 5) + 1
!   if((1.0e-4_wp <= us) .and. (us <= 1.0e-3_wp)) Log_Ave_Counter( 6) = Log_Ave_Counter( 6) + 1
!   if((1.0e-3_wp <= us) .and. (us <= 1.0e-2_wp)) Log_Ave_Counter( 7) = Log_Ave_Counter( 7) + 1
!   if((1.0e-2_wp <= us) .and. (us <= 1.0e-1_wp)) Log_Ave_Counter( 8) = Log_Ave_Counter( 8) + 1
!   if((1.0e-1_wp <= us) .and. (us <= 1.0e-0_wp)) Log_Ave_Counter( 9) = Log_Ave_Counter( 9) + 1
!   if((      eps <= us)                        ) Log_Ave_Counter(10) = Log_Ave_Counter(10) + 1

    if(us <= eps) then
      Logarithmic_Averag = &
!     ave * (c(0)-us*c(1)) / &
!           (d(0)-us*d(1))
!     ave * (c(0)-us*(c(1)-us*c(2))) / &
!           (d(0)-us*(d(1)-us*d(2)))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*c(3)))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*d(3))))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*c(4))))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*d(4)))))
      ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*(c(4)-us*c(5)))))) / &
            (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*(d(4)-us*d(5))))))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*(c(4)-us*(c(5)-us*c(6))))))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*(d(4)-us*(d(5)-us*d(6)))))))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*(c(4)-us*(c(5)-us*(c(6)-us*c(7)))))))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*(d(4)-us*(d(5)-us*(d(6)-us*d(7))))))))
    else
      Logarithmic_Averag = ave * gs / log(xi)
    endif
     loga = Logarithmic_Averag

  end subroutine Logarithmic_Average


subroutine roeavg2000(vlin,vrin,ave)
     use globals, only: gama,neq
  implicit none
  real, intent(in)    :: vlin(neq), vrin(neq)
  real, intent(out)   :: ave(neq)
  real                :: z1r,z2r,z3r,rho_hat
  real                ::z1l,z2l,z3l,z3_bar,z1_bar,z2_bar,z1_log,h_hat
  real                :: u_hat,p1_hat,p2_hat,z3_log,gm1,gp1,twog,a_hat

  gm1  = gama-1.0
  gp1  = gama+1.0
  twog = 2.0*gama

z1l = sqrt(vlin(1)/vlin(3))
z1r = sqrt(vrin(1)/vrin(3))

z2l = vlin(2)*sqrt(vlin(1)/vlin(3))
z2r = vrin(2)*sqrt(vrin(1)/vrin(3))

z3l = sqrt(vlin(1)*vlin(3))
z3r = sqrt(vrin(1)*vrin(3))

z1_bar = 0.5*(z1l + z1r)
z2_bar = 0.5*(z2l + z2r)
z3_bar = 0.5*(z3l + z3r)

u_hat   = z2_bar/z1_bar

call Logarithmic_Average(z3l,z3r,z3_log)
call Logarithmic_Average(z1l,z1r,z1_log)

rho_hat = z1_bar*z3_log
p1_hat  = z3_bar/z1_bar
p2_hat  = (gp1/twog)*(z3_log/z1_log) + (gm1/twog)*(z3_bar/z1_bar)
a_hat   = sqrt(gama*p2_hat/rho_hat)
h_hat   = (a_hat*a_hat)/gm1 + (u_hat*u_hat)/2.0

ave(1) = rho_hat
ave(2) = p1_hat
ave(3) = h_hat
 
 end subroutine roeavg2000