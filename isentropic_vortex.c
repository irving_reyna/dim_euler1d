#include <ssdc.h>

#define DIM 3
#include "navierstokes.h"

enum {dim=DIM, dof=dim+2};
static ns_params _params;
static ns_params *params = &_params;

// dot product of dim sized vectors
static inline ssdc_real_t dot(const ssdc_real_t a[], const ssdc_real_t b[])
{
  int i; ssdc_real_t s = 0;
  for (i=0;i<dim;i++) s += a[i]*b[i];
  return s;
}

#if 0

static void isentropicVortex(const void *ctx,ssdc_real_t V[],const ssdc_real_t xyz[], const ssdc_real_t t)
{
  const ssdc_real_t pi     = PETSC_PI;
  const ssdc_real_t g      = params->gamma0;
  const ssdc_real_t M      = params->Mach0;
  const ssdc_real_t gm1    = g - 1;
  const ssdc_real_t gm1M2  = gm1*M*M;

  ssdc_real_t y0 = 0.0, x = xyz[0];
  ssdc_real_t x0 = 0.0, y = xyz[1];
  ssdc_real_t Uinf = 0;

  ssdc_real_t epsvortex = 5.0;

  ssdc_real_t alpha = 45*pi/180;
  ssdc_real_t ca = cos(alpha);
  ssdc_real_t sa = sin(alpha);

  ssdc_real_t f = 1 - (pow(x-x0-Uinf*ca*t,2) + pow(y-y0-Uinf*sa*t,2));

  // Temperature
  ssdc_real_t T = 1 - pow(epsvortex,2)*gm1M2/(8*pi*pi)*exp(f);

  int j; const int n = dof-1;
  for (j=0; j<dof; j++) V[j] = 0;

  // density
  V[0] = pow(T,1/gm1);
  // velocity (anti-clockwise vortex)
  V[1] = Uinf*ca - epsvortex*(y-y0-Uinf*sa*t)/(2*pi)*exp(f/2);
  V[2] = Uinf*sa + epsvortex*(x-x0-Uinf*ca*t)/(2*pi)*exp(f/2);
  // temperature
  V[n] = T;
}

#else

// https://www.cfd-online.com/Wiki/2-D_vortex_in_isentropic_flow
static void isentropicVortex(const void *ctx,ssdc_real_t V[],const ssdc_real_t xyz[], const ssdc_real_t t)
{
  const ssdc_real_t pi    = PETSC_PI;
  const ssdc_real_t g     = params->gamma0;
  const ssdc_real_t M     = params->Mach0;
  const ssdc_real_t gm1   = g - 1;
  const ssdc_real_t gm1M2 = gm1*M*M;

  ssdc_real_t Uinf = sqrt(2); // Uinf=0.0;

  ssdc_real_t alpha = pi/4;
  ssdc_real_t u_inf = Uinf*cos(alpha);
  ssdc_real_t v_inf = Uinf*sin(alpha);
  ssdc_real_t T_inf = 1;

  ssdc_real_t Cx = 0.0;
  ssdc_real_t Cy = 0.0;
  ssdc_real_t x0 = Cx + u_inf*t;
  ssdc_real_t y0 = Cy + v_inf*t;
  ssdc_real_t x  = xyz[0] - x0;
  ssdc_real_t y  = xyz[1] - y0;
  ssdc_real_t r2 = x*x + y*y;

  ssdc_real_t beta = 5.0;

  // Temperature
 // ssdc_real_t T = T_inf - gm1M2*(beta*beta)/(8*pi*pi)*exp(1-r2);
   ssdc_real_t T = T_inf - gm1*(beta*beta)/(8*pi*pi*g)*exp(1-r2);
  // Density
  ssdc_real_t rho = pow(T,1/gm1);
  // Velocity
  ssdc_real_t u = u_inf - y * beta/(2*pi)*exp((1-r2)/2);
  ssdc_real_t v = v_inf + x * beta/(2*pi)*exp((1-r2)/2);

  int j; const int n = dof-1;
  for (j=0; j<dof; j++) V[j] = 0;

  V[0] = rho;
  V[1] = u;
  V[2] = v;
  V[n] = T;
}

#endif

static void solution(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],ssdc_real_t U[])
{
  ssdc_real_t V[dof];
  isentropicVortex(ctx,V,x,t);
  ns_conservative(ctx,V,U);
}

static void ibc(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],const ssdc_real_t Sn[],ssdc_real_t U[])
{
  solution(params,t,x,U);
}


static void evalSdot(const void *ctx,
                     const ssdc_real_t t,
                     const ssdc_real_t X[],
                     const ssdc_real_t W[],
                     const ssdc_real_t F[],
                     /* */ ssdc_real_t Sdot[])
{
  int j; Sdot[0] = 0;
  for (j=0; j<dof; j++)
    Sdot[0] += W[j]*F[j];
}

static PetscErrorCode MonitorUdot(SSDC ssdc,PetscInt step,PetscReal t,Vec U,PetscReal Udot[],void *ctx)
{
  Vec F;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = SSDCGetWorkVec(ssdc,&F);CHKERRQ(ierr);
  ierr = SSDCFormRHSFunction(ssdc,t,U,F);CHKERRQ(ierr);
  ierr = SSDCComputeIntegral(ssdc,F,Udot);CHKERRQ(ierr);
  ierr = SSDCRestoreVec(ssdc,&F);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode MonitorSdot(SSDC ssdc,PetscInt step,PetscReal t,Vec U,PetscReal Sdot[],void *ctx)
{
  Vec W,F,S;
  SSDC_App app;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = SSDCGetApp(ssdc,&app);CHKERRQ(ierr);
  ierr = SSDCGetWorkVec(ssdc,&W);CHKERRQ(ierr);
  ierr = SSDCGetWorkVec(ssdc,&F);CHKERRQ(ierr);
  ierr = SSDCGetWorkVecBS(ssdc,1,&S);CHKERRQ(ierr);
  ierr = SSDCConvert(ssdc,app->entropy,app->ctx,U,W);CHKERRQ(ierr);
  ierr = SSDCFormRHSFunction(ssdc,t,U,F);CHKERRQ(ierr);
  ierr = SSDCEvaluate(ssdc,evalSdot,NULL,t,W,F,S);CHKERRQ(ierr);
  ierr = SSDCComputeIntegral(ssdc,S,Sdot);CHKERRQ(ierr);
  ierr = SSDCRestoreVecBS(ssdc,&S);CHKERRQ(ierr);
  ierr = SSDCRestoreVec(ssdc,&F);CHKERRQ(ierr);
  ierr = SSDCRestoreVec(ssdc,&W);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char *argv[])
{
  SSDC ssdc;
  MPI_Comm comm;
  PetscLogStage SolveStage = 0;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);
  ierr = PetscLogStageRegister("Solve",&SolveStage);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;

  ierr = SSDCOptionsAlias("-read",NULL,"-ssdc_mesh_read");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-deg",NULL,"-ssdc_deg");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-N","8,8,3","-ssdc_mesh_size");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-W","0,0,1","-ssdc_mesh_wrap");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B","-5,5,-5,5,0,1","-ssdc_mesh_bbox");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B0",NULL,"-ssdc_mesh_bbox_lower");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B1",NULL,"-ssdc_mesh_bbox_upper");CHKERRQ(ierr);

  ierr = SSDCCreate(comm,&ssdc);CHKERRQ(ierr);
  ierr = SSDCSetDim(ssdc,dim);CHKERRQ(ierr);
  ierr = SSDCSetDof(ssdc,dof);CHKERRQ(ierr);
  ierr = SSDCSetName(ssdc,"isentropic_vortex");CHKERRQ(ierr);
  ierr = SSDCSetFromOptions(ssdc);CHKERRQ(ierr);
  ierr = SSDCSetUp(ssdc);CHKERRQ(ierr);

  ierr = SSDCAddField(ssdc,"Density",1,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Velocity",dim,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Temperature",1,-1);CHKERRQ(ierr);

  {
    SSDC_App app;
    ierr = SSDCGetApp(ssdc,&app);CHKERRQ(ierr);

    app->ctx = params;

    app->input   = ns_conservative;
    app->output  = ns_primitive;
    app->entropy = ns_entropy;
    app->primary = ns_primary;

    app->iflux1n = ns_iflux1n;
    app->iflux2n = ns_iflux2n;
    app->upwind  = ns_upwind;

    PetscBool ecf = SSDCGetOptBool(NULL, "-ecflux", PETSC_TRUE);
    if (!ecf) app->iflux   = ns_iflux;
    if (!ecf) app->iflux1n = NULL;
    if (!ecf) app->iflux2n = NULL;

    PetscBool upw = SSDCGetOptBool(NULL, "-upwind", PETSC_TRUE);
    if (!upw) app->upwind = NULL;

    params->gamma0 = SSDCGetOptReal(NULL, "-gamma0", 1.4    );
    params->Mach0  = SSDCGetOptReal(NULL, "-Mach0",  0.5    );
    params->Sfix   = SSDCGetOptReal(NULL, "-Sfix",   1.0e-4 );
    params->Ru0    = SSDCGetOptReal(NULL, "-Ru0",    1.0    );
    ns_setup(params);
  }
  {
    SSDC_Bnd bnd; int index,count; const int *marker;
    ierr = SSDCGetBndMarkers(ssdc,&count,&marker);CHKERRQ(ierr);
    for (index=0; index<count; index++) {
      ierr = SSDCGetBnd(ssdc,marker[index],&bnd);CHKERRQ(ierr);
      bnd->ctx = NULL;
      bnd->ibc = ibc;
    }
  }

  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);

  PetscReal dt = 1e-6, Tf = 5.5;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
  ierr = TSSetMaxTime(ts,Tf);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP);CHKERRQ(ierr);

  ierr = TSSetType(ts,TSRK);CHKERRQ(ierr);
  ierr = TSRKSetType(ts,TSRK5DP);CHKERRQ(ierr);
  ierr = TSSetTolerances(ts,1e-8,NULL,1e-8,NULL);CHKERRQ(ierr);

  ierr = TSSetRHSFunction(ts,NULL,SSDCTSFormRHSFunction,ssdc);CHKERRQ(ierr);

  PetscInt vtk = SSDCGetOptInt(NULL,"-vtk",SSDCHasOptName(NULL,"-vtk"));
  if (vtk) {ierr = SSDCSetTSMonitorVTK(ssdc,ts,NULL,vtk);CHKERRQ(ierr);}

  PetscInt glv = SSDCGetOptInt(NULL,"-glvis",SSDCHasOptName(NULL,"-glvis"));
  if (glv) {ierr = SSDCSetTSMonitorGLVis(ssdc,ts,NULL,PETSC_DEFAULT,glv);CHKERRQ(ierr);}

  PetscInt drw = SSDCGetOptInt(NULL,"-draw",SSDCHasOptName(NULL,"-draw"));
  if (drw) {ierr = SSDCSetTSMonitorDraw(ssdc,ts,NULL,drw);CHKERRQ(ierr);}

  PetscInt lge = SSDCGetOptInt(NULL,"-lg_error",SSDCHasOptName(NULL,"-lg_error"));
  if (lge) {ierr = SSDCSetTSMonitorLGError(ssdc,ts,solution,NULL,lge);CHKERRQ(ierr);}

  PetscInt lgu = SSDCGetOptInt(NULL,"-lg_primary",SSDCHasOptName(NULL,"-lg_primary"));
  if (lgu) {ierr = SSDCSetTSMonitorLG(ssdc,ts,dof,MonitorUdot,NULL,lgu);CHKERRQ(ierr);}

  PetscInt lgs = SSDCGetOptInt(NULL,"-lg_entropy",SSDCHasOptName(NULL,"-lg_entropy"));
  if (lgs) {ierr = SSDCSetTSMonitorLG(ssdc,ts,  1,MonitorSdot,NULL,lgs);CHKERRQ(ierr);}

  {
    Vec U;
    ierr = SSDCCreateVec(ssdc,&U);CHKERRQ(ierr);
    ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
    ierr = VecDestroy(&U);CHKERRQ(ierr);
  }
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  {
    PetscReal t; Vec U;
    ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,NULL,t,U);CHKERRQ(ierr);
  }
  ierr = TSSetUp(ts);CHKERRQ(ierr);

  ierr = PetscLogStagePush(SolveStage);CHKERRQ(ierr);
  ierr = TSSolve(ts,NULL);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  PetscBool error = SSDCGetOptBool(NULL,"-error",PETSC_TRUE);
  if (error) {
    int i, deg; PetscReal t; Vec U,Ue;
    PetscReal errorL1[dof],errorL2[dof],errorMax[dof];
    ierr = TSGetSolveTime(ts,&t);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCGetWorkVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,NULL,t,Ue);CHKERRQ(ierr);
    ierr = SSDCComputeError(ssdc,U,Ue,NULL,errorL1,errorL2,errorMax);CHKERRQ(ierr);
    ierr = SSDCRestoreVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCGetDeg(ssdc,&deg);CHKERRQ(ierr);
    for (i=0; i<dof; i++) {
      //ierr = PetscPrintf(comm,"%.5e\n",(double)errorL2[i]);CHKERRQ(ierr); continue;
      ierr = PetscPrintf(comm,"Error L1: %.5e L2: %.5e Max: %.5e [ dim=%D deg=%D ]\n",
                         (double)errorL1[i],(double)errorL2[i],
                         (double)errorMax[i],dim,deg);CHKERRQ(ierr);
    }
  }

  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = SSDCDestroy(&ssdc);CHKERRQ(ierr);

  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
