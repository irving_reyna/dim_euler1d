#include <ssdc.h>

#define DIM 1

#if !defined(DIM)
#error "DIM not defined"
#endif
#if DIM == 1
#include "navierstokes1d.h"
#define NSPREFIX(symbol) ns1d_##symbol
#endif

#define ns_params        NSPREFIX(params)
#define ns_setup         NSPREFIX(setup)
#define ns_primitive     NSPREFIX(primitive)
#define ns_conservative  NSPREFIX(conservative)
#define ns_entropy       NSPREFIX(entropy)
#define ns_primary       NSPREFIX(primary)
#define ns_grad_w2u      NSPREFIX(grad_w2u)
#define ns_grad_u2w      NSPREFIX(grad_u2w)
#define ns_grad_w2v      NSPREFIX(grad_w2v)
#define ns_grad_v2w      NSPREFIX(grad_v2w)
#define ns_iflux         NSPREFIX(iflux)
#define ns_iflux1n       NSPREFIX(iflux1n)
#define ns_iflux2n       NSPREFIX(iflux2n)
#define ns_upwind        NSPREFIX(upwind)
#define ns_vflux         NSPREFIX(vflux)
#define ns_vfluxn        NSPREFIX(vfluxn)


enum {dim=DIM, dof=dim+2};
static ns_params _params;
static ns_params *params = &_params;

// dot product of dim sized vectors
static inline double dot(const double *a,
                         const double *b) {
  int i; double s = 0;
  for (i=0;i<dim;i++) s += a[i]*b[i];
  return s;
}

static void evalS(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],const ssdc_real_t U[], const ssdc_real_t F[], ssdc_real_t out[])
{
  const int n = dof-1;
  const ssdc_real_t gamma0 = params->gamma0;
  const ssdc_real_t gm1og = (gamma0-1)/gamma0;
  ssdc_real_t V[dof];
  ns_primitive(NULL,U,V);
  out[0] = -V[0]*((1-gm1og)*log(V[n]) - gm1og*log(V[0]));
}


static void rhalf(double xin,double *v,double alpha,double vf){
  //solve equation by interval halving
  //f = EXP(2*(1-VF)*X/ALPH)-(V-1)**2/ABS(VF-V)**(2*VF)
  //f = EXP(-2*(1-VF)*X/ALPH)-ABS(VF-V)**(2*VF)/(V-1)**2

  double eps,vh,vl,vm,fm,tmp1,tmp2,tmp;
  int i;

  eps  = 1e-12;
  vh   = 1.;
  vl   = vf;
  vm   = (vh+vl)*0.5;
  //fh   = exp(2.*(1.-vf)*xin/alpha)-(vh-1.)**2/fabs(vf-vh)**(2.*vf);

  if (xin < 0.) {
    fm   = exp(2.*(1.-vf)*xin/alpha)-pow(vm-1.,2.)/pow(fabs(vf-vm),(2.*vf));}
  else{
    fm   = -exp(-2.*(1.-vf)*xin/alpha)+pow(fabs(vf-vm),(2.*vf))/pow(vm-1.,2.);}

  //fl   = exp(-2.*(1.-vf)*xin/alpha)-fabs(vf-vl)**(2.*vf)/(vl-1.)**2;

  if(fm < 0.) {
    //fl = fm;
    vl = vm;

    for(i = 0;i < 200;i++){
      vm = (vh+vl)*0.5;
      fm = exp(2.0*(1.0-vf)*xin/alpha)-pow(vm-1.0,2.)/pow(fabs(vf-vm),(2.*vf));
      tmp = (vm - vf) * (1. - vm);
      if(tmp < 0.){tmp = -1.*tmp;}
      if(tmp == 0.){tmp = 1.0e-14;}
      tmp1 = (1.-vm);
      if(tmp1 < 0.){tmp1=1.0e-14;}
      tmp2 = (vm-vf);
      if(tmp2 < 0.){tmp2=1.0e-14;}
      fm=xin-alpha*(log(tmp)+(vf+1.)*log(tmp1/tmp2)/(1.-vf))*0.5;
      if(fm < 0.){
        //fl = fm;
        vl = vm;}
      else{
        //fh = fm;
        vh = vm;}
      if(fabs(fm) < eps){break;}
    }}
  else{
    //fh = fm;
    vh = vm;

    for(i = 0;i<100;i++){
      vm = (vh+vl)*0.5;
      fm =-exp(-2.*(1.-vf)*xin/alpha)+pow(fabs(vf-vm),(2.*vf))/pow(vm-1.,2.);
      if(fm < 0.){
        //fl = fm;
        vl = vm;}
      else{
        //fh = fm;
        vh = vm;}
      if(fabs(fm) < eps){break;}
    }
  }
  *v = vm;
}

static void viscousShock(const void *ctx,double Vx[],double dWdx[],
                         const double xin[],const double tin){

  double Mach0  = params->Mach0;
  double gamma0 = params->gamma0;
  double Ru0    = params->Ru0;

  double gm1   = gamma0 - 1.;
  double gm1M2 = gm1*Mach0*Mach0;
  double Uinf;
  double f, xp;
  double alph, vf, mdot, wave, M2, uhat, Rloc;
  //double mu = 1.;//0.000018;
  double mu = 0.000018;

  double k =  2.0*gamma0*Ru0*4.*mu/(3.0*gm1);
  double w = 8314.472/287.058;   // R/Re
  double n = 1.0;//*6.022140857*pow(10,23) ;
  double kb = 1.38064852*pow(10.0,-23);
  double k1 = n*w/(3.0*kb);

  int i;

  double x0[dim];
  for(i=0;i<dim;i++) x0[i] = 0.5;

  Uinf = 1.;
  M2 = Mach0*Mach0; //Uinf*Uinf
  mdot = fabs(Uinf);

  double direction[3] = {1,1,1};

  double vec[dim];
  double norm = sqrt(dot(direction,direction));
  for(i=0;i<dim;i++) vec[i] = direction[i]/norm;

  // project position into vec
  double xx[dim];
  for(i=0;i<dim;i++) xx[i] = xin[i]-x0[i];
  xp = dot(vec,xx);

  for(i=0;i<dof;i++)
    Vx[i] = 0;

  // set mixture variable
  Rloc = 1.0;

  double referencewavespeed = 0.25;//*pow(1.0,10);
  wave = referencewavespeed;

  // Set dynamic viscosity
  //if (variable_mu)
  //  sutherland_law(Vx[4],mu);

  // Set heat conductivity

 // vf = (gm1+2./M2)/(gamma0+1.);  //Correction Irving

  vf   = (gm1+2.0*Ru0*gamma0)/(gamma0+1.);
  alph = (4.*mu*(gamma0+1.))/(6.0*gamma0*mdot);

//  alph = (4./3.*mu/Ru0)/mdot*(2.*gamma0)/(gamma0+1.);

  rhalf(xp-wave*tin,&f,alph,vf);
  uhat = f*Uinf;

  // density
  double rho = fabs(mdot)/f;
  // velocity
  double Vel = (uhat+wave);
  // Temperature
  //double T = (Uinf*Uinf + gm1M2*0.5*(Uinf*Uinf-uhat*uhat))/Rloc;    //Correction Irving
  double T = Uinf*Uinf + (gm1/(2.0*gamma0*Ru0))*(Uinf*Uinf-uhat*uhat);    //Correction Irving

  Vx[0] = rho;
  for(i=0;i<dim;i++) Vx[i+1] = Vel*vec[i];
  Vx[dof-1] = T;

  // gradients are not needed
  // derivative of f w.r.t. direction of shock
  /* double f_xp = (f-1)*(f-vf)/(alph*f); */

  /* // gradients in shock coordinates */
  /* double rho_xp = -rho/f * f_xp; */
  /* double v_xp = Uinf * f_xp; */
  /* double T_xp = -0.5*gm1M2*Uinf*Uinf*2*f/Rloc * f_xp; */

  /* // Gradient of density in mesh coordinates */
  /* double rho_x[dim]; */
  /* for (i=0;i<dim;i++) rho_x[i] = rho_xp*vec[i]; */

  /* // This one is fun: Gradient of velocity in mesh coordinates */
  /* double v_x[dim][dim]; */
  /* for(i=0;i<dim;i++) */
  /*   for(j=0;j<dim;j++) */
  /*     v_x[i][j] = v_xp*vec[i]*vec[j]; */

  /* // Gradient of temperature in mesh coordinates */
  /* double T_x[dim]; */
  /* for (i=0;i<dim;i++) T_x[i] = T_xp*vec[i]; */

  /* // full gradient in primitive variables */
  /* double dVdx[dof][dim]; */
  /* for(i=0;i<dim;i++) */
  /*   dVdx[0][i] = rho_x[i]; */
  /* for(i=0;i<dim;i++) */
  /*   for(j=0;j<dim;j++) */
  /*     dVdx[i+1][j] = v_x[i][j]; */
  /* for(i=0;i<dim;i++) */
  /*   dVdx[dof-1][i] = T_x[i]; */
}

static void solution(const void *ctx,double t,const double x[],double U[])
{
  double V[dof],G[dof*dim];
  viscousShock(ctx,V,G,x,t);
  ns_conservative(ctx,V,U);
}

// inviscid part of the boundary condition
static void bcI(const void *ctx,
                const ssdc_real_t t,
                const ssdc_real_t x[],
                const ssdc_real_t Sn[],
                ssdc_real_t U[])
{
#if 1
  solution(ctx,t,x,U);
#else
  int i;
  // Dirichlet boundary so no gradients!
  ssdc_real_t U_analytic[dof];
  solution(ctx,t,x,U_analytic);
  ssdc_real_t V_analytic[dof];
  ns_primitive(params,U_analytic,V_analytic);

  ssdc_real_t Um[dof],Vm[dof];
  ssdc_real_t Up[dof],Vp[dof];

  for(i=0;i<dof;i++) Um[i] = U[i];
  ns_primitive(params,Um,Vm);
  for (i=0;i<dof;i++)
    Vp[i] = -Vm[i] + 2 * V_analytic[i];
    //Vp[i] = V_analytic[i];
  ns_conservative(params,Vp,Up);
  for(i=0;i<dof;i++) U[i] = Up[i];
#endif
}


int main(int argc, char *argv[])
{
  SSDC ssdc;
  MPI_Comm comm;
  PetscLogStage SolveStage = 0;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);
  ierr = PetscLogStageRegister("Solve",&SolveStage);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;

  ierr = SSDCOptionsAlias("-deg",NULL,"-ssdc_deg");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-N","8,8,3","-ssdc_mesh_size");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-W","0,0,0","-ssdc_mesh_wrap");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B","0,1,0,1,0,1","-ssdc_mesh_bbox");CHKERRQ(ierr);

  ierr = SSDCCreate(comm,&ssdc);CHKERRQ(ierr);
  ierr = SSDCSetDim(ssdc,dim);CHKERRQ(ierr);
  ierr = SSDCSetDof(ssdc,dof);CHKERRQ(ierr);
  ierr = SSDCSetName(ssdc,"test1d");CHKERRQ(ierr);
  ierr = SSDCSetFromOptions(ssdc);CHKERRQ(ierr);
  ierr = SSDCSetUp(ssdc);CHKERRQ(ierr);

  ierr = SSDCAddField(ssdc,"Density",1,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Velocity",dim,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Temperature",1,-1);CHKERRQ(ierr);

  {
    SSDC_App app;
    ierr = SSDCGetApp(ssdc,&app);CHKERRQ(ierr);

    app->ctx = params;

    app->input   = ns_conservative;
    app->output  = ns_primitive;
    app->entropy = ns_entropy;
    app->primary = ns_primary;

    app->iflux1n = ns_iflux1n;
    app->iflux2n = ns_iflux2n;
    app->upwind  = ns_upwind;
    app->vflux   = ns_vflux;

    params->Mach0  = SSDCGetOptReal(NULL, "-Mach0"  , 2.5    ); // viscous shock
    //params->Mach0  = SSDCGetOptReal(NULL, "-Mach0"  , 0.8451542547285165); // SOD
    params->gamma0 = SSDCGetOptReal(NULL, "-gamma0" , 1.4    );
    params->Ru0    = SSDCGetOptReal(NULL, "-Ru0"    , 1.0  ); // viscous shock
    params->Pr0    = SSDCGetOptReal(NULL, "-Pr0"    , 0.75   ); // viscous shock
    params->Sfix   = SSDCGetOptReal(NULL, "-Sfix"   , 0.0    );
    ns_setup(params);
  }
  {
    SSDC_Bnd bnd; int index,count; const int *marker;
    ierr = SSDCGetBndMarkers(ssdc,&count,&marker);CHKERRQ(ierr);
    for (index=0; index<count; index++) {
      ierr = SSDCGetBnd(ssdc,marker[index],&bnd);CHKERRQ(ierr);
      bnd->ctx = NULL;
      bnd->ibc = bcI;
    }
  }


  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);

  { /* Set the solution vector in advance */
    Vec U;
    ierr = SSDCCreateVec(ssdc,&U);CHKERRQ(ierr);
    ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
    ierr = VecDestroy(&U);CHKERRQ(ierr);
  }

  { /* Basic timestepper configuration */
    PetscReal dt = 1e-6;
    PetscReal Tf = 0.1;
    ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
    ierr = TSSetMaxTime(ts,Tf);CHKERRQ(ierr);
    ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP);CHKERRQ(ierr);
    ierr = TSSetTolerances(ts,1e-8,NULL,1e-8,NULL);CHKERRQ(ierr);
  }

  PetscBool im = SSDCGetOptBool(NULL,"-implicit",PETSC_FALSE);
  if (!im) {
    ierr = TSSetType(ts,TSRK);CHKERRQ(ierr);
    ierr = TSRKSetType(ts,TSRK5DP);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,NULL,SSDCTSFormRHSFunction,ssdc);CHKERRQ(ierr);
  } else {
    Mat mat;
    ierr = SSDCCreateMat(ssdc,&mat);CHKERRQ(ierr);
    ierr = TSSetType(ts,TSBDF);CHKERRQ(ierr);
    ierr = TSBDFSetOrder(ts,2);CHKERRQ(ierr);
    ierr = TSSetIFunction(ts,NULL,SSDCTSFormIFunction,ssdc);CHKERRQ(ierr);
    ierr = TSSetIJacobian(ts,mat,mat,SSDCTSFormIJacobian,ssdc);CHKERRQ(ierr);
    ierr = MatDestroy(&mat);CHKERRQ(ierr);
  }

  /* Add monitors to timestepper */
  PetscInt stt = SSDCGetOptInt(NULL,"-state",SSDCHasOptName(NULL,"-state"));
  if (stt) {ierr = SSDCSetTSMonitorState(ssdc,ts,NULL,stt);CHKERRQ(ierr);}

  PetscInt glv = SSDCGetOptInt(NULL,"-glvis",SSDCHasOptName(NULL,"-glvis"));
  if (glv) {ierr = SSDCSetTSMonitorGLVis(ssdc,ts,NULL,PETSC_DEFAULT,glv);CHKERRQ(ierr);}

  PetscInt vtk = SSDCGetOptInt(NULL,"-vtk",SSDCHasOptName(NULL,"-vtk"));
  if (vtk) {ierr = SSDCSetTSMonitorVTK(ssdc,ts,NULL,vtk);CHKERRQ(ierr);}

  PetscInt drw = SSDCGetOptInt(NULL,"-draw",SSDCHasOptName(NULL,"-draw"));
  if (drw) {ierr = SSDCSetTSMonitorDraw(ssdc,ts,NULL,drw);CHKERRQ(ierr);}


  PetscInt lgs = SSDCGetOptInt(NULL,"-lg_entropy",SSDCHasOptName(NULL,"-lg_entropy"));
  if (lgs) {ierr = SSDCSetTSMonitorLGIntegral(ssdc,ts,1,evalS,params,lgs);CHKERRQ(ierr);}


  /* Configure timestepper from command line */
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);

  PetscBool load = SSDCHasOptName(NULL,"-load_state");
  if (!load) {
    /* Compute initial solution */
    PetscReal t; Vec U;
    ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,params,t,U);CHKERRQ(ierr);
  } else {
    /* Restart from previous state */
    const char *filename = SSDCGetOptString(NULL,"-load_state","state.dat");
    PetscInt step; PetscReal time; Vec U;
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCReadState(ssdc,filename,&step,&time,U);CHKERRQ(ierr);
    ierr = TSSetStepNumber(ts,step);CHKERRQ(ierr);
    ierr = TSSetTime(ts,time);CHKERRQ(ierr);
  }

  /* Solve */
  ierr = PetscLogStagePush(SolveStage);CHKERRQ(ierr);
  ierr = TSSolve(ts,NULL);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  PetscBool save = SSDCHasOptName(NULL,"-save_state");
  if (save) {
    /* Checkpoint final state */
    const char *filename = SSDCGetOptString(NULL,"-save_state","state.dat");
    PetscInt step; PetscReal time; Vec U;
    ierr = TSGetStepNumber(ts,&step);CHKERRQ(ierr);
    ierr = TSGetTime(ts,&time);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCWriteState(ssdc,filename,step,time,U);CHKERRQ(ierr);
  }

  PetscBool error = SSDCGetOptBool(NULL,"-error",PETSC_FALSE);
  if (error) {
    PetscInt  deg;
    PetscReal t; Vec U,Ue;
    PetscReal errorL1[dof],errorL2[dof],errorMax[dof];
    ierr = TSGetSolveTime(ts,&t);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCGetWorkVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,params,t,Ue);CHKERRQ(ierr);
    ierr = SSDCComputeError(ssdc,U,Ue,NULL,errorL1,errorL2,errorMax);CHKERRQ(ierr);
    ierr = SSDCRestoreVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCGetDeg(ssdc,&deg);CHKERRQ(ierr);
    int i;
    for(i=0 ; i<dof ; i++){
      ierr = PetscPrintf(comm,"Error L1: %.5e L2: %.5e Max: %.5e [ dim=%D deg=%D ]\n",
                         (double)errorL1[i],(double)errorL2[i],
                         (double)errorMax[i],dim,deg);CHKERRQ(ierr);
    }
  }

  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = SSDCDestroy(&ssdc);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
