#include <ssdc.h>

#define DIM 1
#include "navierstokes.h"

enum {dim=DIM, dof=dim+2};
static ns_params _params;
static ns_params *params = &_params;

// dot product of dim sized vectors
static inline ssdc_real_t dot(const ssdc_real_t *a,const ssdc_real_t *b)
{
  int i; ssdc_real_t s = 0;
  for (i=0;i<dim;i++) s += a[i]*b[i];
  return s;
}

static void rhalf(ssdc_real_t xin,ssdc_real_t *v,ssdc_real_t alpha,ssdc_real_t vf){
  //solve equation by interval halving
  //f = EXP(2*(1-VF)*X/ALPH)-(V-1)**2/ABS(VF-V)**(2*VF)
  //f = EXP(-2*(1-VF)*X/ALPH)-ABS(VF-V)**(2*VF)/(V-1)**2

  ssdc_real_t eps,vh,vl,vm,fm,tmp1,tmp2,tmp;
  int i;

  eps  = 1e-12;
  vh   = 1.;
  vl   = vf;
  vm   = (vh+vl)*0.5;
  //fh   = exp(2.*(1.-vf)*xin/alpha)-(vh-1.)**2/fabs(vf-vh)**(2.*vf);

  if (xin < 0.) {
    fm   = exp(2.*(1.-vf)*xin/alpha)-pow(vm-1.,2.)/pow(fabs(vf-vm),(2.*vf));}
  else{
    fm   = -exp(-2.*(1.-vf)*xin/alpha)+pow(fabs(vf-vm),(2.*vf))/pow(vm-1.,2.);}

  //fl   = exp(-2.*(1.-vf)*xin/alpha)-fabs(vf-vl)**(2.*vf)/(vl-1.)**2;

  if(fm < 0.) {
    //fl = fm;
    vl = vm;

    for(i = 0;i < 200;i++){
      vm = (vh+vl)*0.5;
      fm = exp(2.0*(1.0-vf)*xin/alpha)-pow(vm-1.0,2.)/pow(fabs(vf-vm),(2.*vf));
      tmp = (vm - vf) * (1. - vm);
      if(tmp < 0.){tmp = -1.*tmp;}
      if(tmp == 0.){tmp = 1.0e-14;}
      tmp1 = (1.-vm);
      if(tmp1 < 0.){tmp1=1.0e-14;}
      tmp2 = (vm-vf);
      if(tmp2 < 0.){tmp2=1.0e-14;}
      fm=xin-alpha*(log(tmp)+(vf+1.)*log(tmp1/tmp2)/(1.-vf))*0.5;
      if(fm < 0.){
        //fl = fm;
        vl = vm;}
      else{
        //fh = fm;
        vh = vm;}
      if(fabs(fm) < eps){break;}
    }}
  else{
    //fh = fm;
    vh = vm;

    for(i = 0;i<100;i++){
      vm = (vh+vl)*0.5;
      fm =-exp(-2.*(1.-vf)*xin/alpha)+pow(fabs(vf-vm),(2.*vf))/pow(vm-1.,2.);
      if(fm < 0.){
        //fl = fm;
        vl = vm;}
      else{
        //fh = fm;
        vh = vm;}
      if(fabs(fm) < eps){break;}
    }
  }
  *v = vm;
}

ssdc_real_t direction[] = {1,0,0};

static void viscousShock(const void *ctx,ssdc_real_t Vx[],
                         const ssdc_real_t xin[],const ssdc_real_t tin)
{

  ssdc_real_t gamma0 = params->gamma0;
  ssdc_real_t Mach0  = params->Mach0;
  ssdc_real_t Re0    = params->Re0;


  ssdc_real_t gm1   = gamma0 - 1.;
  ssdc_real_t gm1M2 = gm1*Mach0*Mach0;
  ssdc_real_t Uinf;
  ssdc_real_t f, xp;
  ssdc_real_t alph, vf, mdot, wave, M2, uhat, Rloc;
  ssdc_real_t mu = 1.0;

  int i; const int n=dof-1;

  //ssdc_real_t x0[3] = {0.5,0.5,0.5};
  ssdc_real_t x0[dim];
  for(i=0;i<dim;i++) x0[i] = 0.0;

  Uinf = 1.;
  M2 = Mach0*Mach0; //Uinf*Uinf
  mdot = fabs(Uinf);

  ssdc_real_t vec[dim];
  ssdc_real_t norm = sqrt(dot(direction,direction));
  for(i=0;i<dim;i++) vec[i] = direction[i]/norm;

  // project position into vec
  ssdc_real_t xx[dim];
  for(i=0;i<dim;i++) xx[i] = xin[i]-x0[i];
  xp = dot(vec,xx);

  for(i=0;i<dof;i++)
    Vx[i] = 0;

  // set mixture variable
  Rloc = 1.0;

  ssdc_real_t referencewavespeed = 0.25;
  wave = referencewavespeed;

  // Set dynamic viscosity
  //if (variable_mu)
  //  sutherland_law(Vx[4],mu);

  // Set heat conductivity

  vf = (gm1+2./M2)/(gamma0+1.);

  alph = (4./3.*mu/Re0)/mdot*(2.*gamma0)/(gamma0+1.);

  rhalf(xp-wave*tin,&f,alph,vf);
  uhat = f*Uinf;

  // density
  ssdc_real_t rho = fabs(mdot)/f;
  // velocity
  ssdc_real_t Vel = (uhat+wave);
  // Temperature
  ssdc_real_t T = (Uinf*Uinf + gm1M2*0.5*(Uinf*Uinf-uhat*uhat))/Rloc;

  Vx[0] = rho;
  for(i=0;i<dim;i++) Vx[i+1] = Vel*vec[i];
  Vx[n] = T;

/*
  // derivative of f w.r.t. direction of shock
  ssdc_real_t f_xp = (f-1)*(f-vf)/(alph*f);

  // gradients in shock coordinates
  ssdc_real_t rho_xp = -rho/f * f_xp;
  ssdc_real_t v_xp = Uinf * f_xp;
  ssdc_real_t T_xp = -0.5*gm1M2*Uinf*Uinf*2*f/Rloc * f_xp;

  // Gradient of density in mesh coordinates
  ssdc_real_t rho_x[dim];
  for (i=0;i<dim;i++) rho_x[i] = rho_xp*vec[i];

  // This one is fun: Gradient of velocity in mesh coordinates
  ssdc_real_t v_x[dim][dim];
  for(i=0;i<dim;i++)
    for(j=0;j<dim;j++)
      v_x[i][j] = v_xp*vec[i]*vec[j];

  // Gradient of temperature in mesh coordinates
  ssdc_real_t T_x[dim];
  for (i=0;i<dim;i++) T_x[i] = T_xp*vec[i];

  // full gradient in primitive variables
  ssdc_real_t dVdx[dof][dim];
  for(i=0;i<dim;i++)
    dVdx[0][i] = rho_x[i];
  for(i=0;i<dim;i++)
    for(j=0;j<dim;j++)
      dVdx[i+1][j] = v_x[i][j];
  for(i=0;i<dim;i++)
    dVdx[dof-1][i] = T_x[i];

  // gradient of entropy variables WRT primitive variables
  ssdc_real_t Ux[dof],dWdV[dof][dof];
  ns_conservative(ctx,Vx,Ux);
  ns_dwdv(ctx,Ux,&dWdV[0][0]);

  // Do a mmult to move the gradient from primitive to entropy variables
  for(i=0;i<dof;i++){
    for(j=0;j<dim;j++){
      ssdc_real_t sum = 0;
      for(k=0;k<dof;k++)
        sum += dWdV[i][k]*dVdx[k][j];
      dWdx[i*dim+j] = sum;
    }
  }
*/
}

static void solution(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],ssdc_real_t U[])
{
  ssdc_real_t V[dof];
  viscousShock(ctx,V,x,t);
  ns_conservative(params,V,U);
}

static void ibc(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],const ssdc_real_t Sn[],ssdc_real_t U[])
{
  solution(ctx,t,x,U);
}

static void vbc(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],const ssdc_real_t Sn[],ssdc_real_t U[],ssdc_real_t G[],ssdc_real_t fVn[])
{
  int j;

  ssdc_real_t Ub[dof];
  solution(ctx,t,x,Ub);

  ssdc_real_t Um[dof];
  ssdc_real_t Up[dof];
  for(j=0;j<dof;j++) Um[j] = U[j];
  for (j=0; j<dof; j++)
    Up[j] = -Um[j] + 2 * Ub[j];
  for(j=0;j<dof;j++) U[j] = Up[j];

  ssdc_real_t Gu[dof*dim];
  ns_grad_w2u(params,Um,G,Gu);
  ns_grad_u2w(params,Up,Gu,G);
}

int main(int argc, char *argv[])
{
  SSDC ssdc;
  MPI_Comm comm;
  PetscLogStage SolveStage = 0;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);
  ierr = PetscLogStageRegister("Solve",&SolveStage);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;

  PetscInt ndir=dim;
  ierr = PetscOptionsGetRealArray(NULL,NULL,"-dir",direction,&ndir,NULL);CHKERRQ(ierr);

  ierr = SSDCOptionsAlias("-read",NULL,"-ssdc_mesh_read");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-deg",NULL,"-ssdc_deg");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-N","8,3,3","-ssdc_mesh_size");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-W","0,0,0","-ssdc_mesh_wrap");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B","0,1,0,1,0,1","-ssdc_mesh_bbox");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B0",NULL,"-ssdc_mesh_bbox_lower");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-B1",NULL,"-ssdc_mesh_bbox_upper");CHKERRQ(ierr);

  ierr = SSDCCreate(comm,&ssdc);CHKERRQ(ierr);
  ierr = SSDCSetDim(ssdc,dim);CHKERRQ(ierr);
  ierr = SSDCSetDof(ssdc,dof);CHKERRQ(ierr);
  ierr = SSDCSetFromOptions(ssdc);CHKERRQ(ierr);
  ierr = SSDCSetUp(ssdc);CHKERRQ(ierr);

  ierr = SSDCAddField(ssdc,"Density",1,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Velocity",dim,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Pressure",1,-1);CHKERRQ(ierr);

  {
    SSDC_App app;
    ierr = SSDCGetApp(ssdc,&app);CHKERRQ(ierr);

    app->ctx = params;

    app->input   = ns_conservative;
    app->output  = ns_primitive;
    app->entropy = ns_entropy;
    app->primary = ns_primary;

    app->iflux1n = ns_iflux1n;
    app->iflux2n = ns_iflux2n;
    app->upwind  = ns_upwind;
    app->vflux   = ns_vflux;

    params->gamma0 = SSDCGetOptReal(NULL, "-gamma0" ,  1.4  );
    params->Mach0  = SSDCGetOptReal(NULL, "-Mach0"    ,  1.0  );
    params->Re0    = SSDCGetOptReal(NULL, "-Re0"    , 1.0  );
    params->Pr0    = SSDCGetOptReal(NULL, "-Pr0"    ,  1.0 );
    params->Sfix   = SSDCGetOptReal(NULL, "-Sfix"   ,  0.0  );
    params->Cp     = SSDCGetOptReal(NULL, "-Cp"    ,  1.0 );
    params->Cv     = SSDCGetOptReal(NULL, "-Cv"   ,  1.0  );
    ns_setup(params);
  }
  {
    SSDC_Bnd bnd; int index,count; const int *marker;
    ierr = SSDCGetBndMarkers(ssdc,&count,&marker);CHKERRQ(ierr);
    for (index=0; index<count; index++) {
      ierr = SSDCGetBnd(ssdc,marker[index],&bnd);CHKERRQ(ierr);
      bnd->ctx = NULL;
      bnd->ibc = ibc;
      bnd->vbc = vbc;
    }
  }

  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);

  PetscReal dt = 1e-20;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
  ierr = TSSetMaxTime(ts,0.9);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP);CHKERRQ(ierr);

  ierr = TSSetType(ts,TSRK);CHKERRQ(ierr);
  ierr = TSRKSetType(ts,TSRK5DP);CHKERRQ(ierr);
  ierr = TSSetTolerances(ts,1e-8,NULL,1e-8,NULL);CHKERRQ(ierr);

  ierr = TSSetRHSFunction(ts,NULL,SSDCTSFormRHSFunction,ssdc);CHKERRQ(ierr);

  PetscInt vtk = SSDCGetOptInt(NULL,"-vtk",SSDCHasOptName(NULL,"-vtk"));
  if (vtk) {ierr = SSDCSetTSMonitorVTK(ssdc,ts,"isentropic_vortex",vtk);CHKERRQ(ierr);}

  PetscInt glv = SSDCGetOptInt(NULL,"-glvis",SSDCHasOptName(NULL,"-glvis"));
  if (glv) {ierr = SSDCSetTSMonitorGLVis(ssdc,ts,NULL,PETSC_DEFAULT,glv);CHKERRQ(ierr);}

  PetscInt lge = SSDCGetOptInt(NULL,"-lg_error",SSDCHasOptName(NULL,"-lg_error"));
  if (lge) {ierr = SSDCSetTSMonitorLGError(ssdc,ts,solution,params,lge);CHKERRQ(ierr);}

    PetscInt drw = SSDCGetOptInt(NULL,"-draw",SSDCHasOptName(NULL,"-draw"));
  if (drw) {ierr = SSDCSetTSMonitorDraw(ssdc,ts,NULL,drw);CHKERRQ(ierr);}


  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  {
    PetscReal t; Vec U;
    ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
    ierr = SSDCCreateVec(ssdc,&U);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,params,t,U);CHKERRQ(ierr);
    ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
    ierr = VecDestroy(&U);CHKERRQ(ierr);
  }

  ierr = TSSetUp(ts);CHKERRQ(ierr);

  ierr = PetscLogStagePush(SolveStage);CHKERRQ(ierr);
  ierr = TSSolve(ts,NULL);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  PetscBool error = SSDCGetOptBool(NULL,"-error",PETSC_TRUE);
  if (error) {
    int i,deg;
    PetscReal t; Vec U,Ue;
    PetscReal errorL1[dof],errorL2[dof],errorMax[dof];
    ierr = TSGetSolveTime(ts,&t);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCGetWorkVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,params,t,Ue);CHKERRQ(ierr);
    ierr = SSDCComputeError(ssdc,U,Ue,NULL,errorL1,errorL2,errorMax);CHKERRQ(ierr);
    ierr = SSDCRestoreVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCGetDeg(ssdc,&deg);CHKERRQ(ierr);
    for (i=0; i<dof; i++) {
      ierr = PetscPrintf(comm,"Error L1: %.5e L2: %.5e Max: %.5e [ dim=%D deg=%D ]\n",
                         (double)errorL1[i],(double)errorL2[i],
                         (double)errorMax[i],dim,deg);CHKERRQ(ierr);
    }
  }

  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = SSDCDestroy(&ssdc);CHKERRQ(ierr);

  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
