#include <ssdc.h>

#define DIM 1
#include "navierstokes.h"

enum {dim=DIM, dof=dim+2};
static ns_params _params;
static ns_params *params = &_params;

static void initial_sod(const void *ctx,double xin, double U[])
{
  int i; const int n = dof-1;
  double Vl[dof];
  double Vr[dof];
  for (i=0;i<dof;i++)
    Vl[i] = Vr[i] = 0;

  Vl[0] = 1.0;
  Vl[n] = 1.0;
  Vr[0] = 0.125;
  Vr[n] = 0.8;

  double Ul[dof],Ur[dof];
  ns_conservative(ctx,Vl,Ul);
  ns_conservative(ctx,Vr,Ur);
  for(i=0;i<dof;i++)
    U[i] = (xin<0.5) ? Ul[i] : Ur[i];
}

static void solution(const void *ctx,double t,const double x[],double U[])
{
  initial_sod(ctx,x[0],U);
}

static void ibc(const void *ctx,double t,const double x[],const double Jn[],double U[])
{
  solution(ctx,t,x,U);
}

static void evalU(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],const ssdc_real_t U[], const ssdc_real_t F[], ssdc_real_t out[])
{
  int j; for (j=0; j<dof; j++) out[j] = U[j];
 // out[0] = U[1];
}

static void evalS(const void *ctx,ssdc_real_t t,const ssdc_real_t x[],const ssdc_real_t U[], const ssdc_real_t F[], ssdc_real_t out[])
{
  const int n = dof-1;
  const ssdc_real_t gamma0 = params->gamma0;
  const ssdc_real_t Ru0 = params->Ru0;
  ssdc_real_t V[dof];
  ns_primitive(NULL,U,V);
  out[0] = -V[0]*( log(V[n]) - gamma0*log(V[0]) );
}

int main(int argc, char *argv[])
{
  SSDC ssdc;
  MPI_Comm comm;
  PetscLogStage SolveStage = 0;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc,&argv,NULL,NULL);CHKERRQ(ierr);
  ierr = PetscLogStageRegister("Solve",&SolveStage);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;

  ierr = SSDCOptionsAlias("-deg",NULL,"-ssdc_deg");CHKERRQ(ierr);
  ierr = SSDCOptionsAlias("-N","32,0,0","-ssdc_mesh_size");CHKERRQ(ierr);

  ierr = SSDCCreate(comm,&ssdc);CHKERRQ(ierr);
  ierr = SSDCSetDim(ssdc,dim);CHKERRQ(ierr);
  ierr = SSDCSetDof(ssdc,dof);CHKERRQ(ierr);
  ierr = SSDCSetName(ssdc,"sod_tube");CHKERRQ(ierr);
  ierr = SSDCSetFromOptions(ssdc);CHKERRQ(ierr);
  ierr = SSDCSetUp(ssdc);CHKERRQ(ierr);

  ierr = SSDCAddField(ssdc,"Density",1,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Velocity",dim,-1);CHKERRQ(ierr);
  ierr = SSDCAddField(ssdc,"Pressure",1,-1);CHKERRQ(ierr);

  {
    SSDC_App app;
    ierr = SSDCGetApp(ssdc,&app);CHKERRQ(ierr);

    app->ctx = params;

  app->input   = ns_conservative;
  app->output  = ns_primitive;
  app->entropy = ns_entropy;
  app->primary = ns_primary; 
  
    
  app->iflux1n = ns_iflux1n;
  app->iflux2n = ns_iflux2n;
  app->upwind  = ns_upwind;
  //  app->iflux   = ns_iflux;
   //   app->upwind  = ns_upwind;

    PetscBool upw = SSDCGetOptBool(NULL, "-upwind", PETSC_TRUE);
    if (!upw) app->upwind = NULL;

    ssdc_real_t Mach0 = sqrt(5.0/7.0);
    params->gamma0 = SSDCGetOptReal(NULL, "-gamma0" , 1.4    );
    params->Mach0  = SSDCGetOptReal(NULL, "-Mach0"  , Mach0  );
    params->Sfix   = SSDCGetOptReal(NULL, "-Sfix"   , 1.0e-4 );
    params->Ru0    = SSDCGetOptReal(NULL, "-Ru0"    , 1.0  );

    ns_setup(params);
  }
  {
    SSDC_Bnd bnd; int index,count; const int *marker;
    ierr = SSDCGetBndMarkers(ssdc,&count,&marker);CHKERRQ(ierr);
    for (index=0; index<count; index++) {
      ierr = SSDCGetBnd(ssdc,marker[index],&bnd);CHKERRQ(ierr);
      bnd->ctx = NULL;
      bnd->ibc = NULL;
    }
  }

  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);

  PetscReal dt = 1e-6, Tf = 0.2;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
  ierr = TSSetMaxTime(ts,Tf);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP);CHKERRQ(ierr);

  ierr = TSSetType(ts,TSRK);CHKERRQ(ierr);
  ierr = TSRKSetType(ts,TSRK5DP);CHKERRQ(ierr);
  ierr = TSSetTolerances(ts,1e-8,NULL,1e-8,NULL);CHKERRQ(ierr);

  ierr = TSSetRHSFunction(ts,NULL,SSDCTSFormRHSFunction,ssdc);CHKERRQ(ierr);

  PetscInt vtk = SSDCGetOptInt(NULL,"-vtk",SSDCHasOptName(NULL,"-vtk"));
  if (vtk) {ierr = SSDCSetTSMonitorVTK(ssdc,ts,NULL,vtk);CHKERRQ(ierr);}

  PetscInt glv = SSDCGetOptInt(NULL,"-glvis",SSDCHasOptName(NULL,"-glvis"));
  if (glv) {ierr = SSDCSetTSMonitorGLVis(ssdc,ts,NULL,PETSC_DEFAULT,glv);CHKERRQ(ierr);}

  PetscInt drw = SSDCGetOptInt(NULL,"-draw",SSDCHasOptName(NULL,"-draw"));
  if (drw) {ierr = SSDCSetTSMonitorDraw(ssdc,ts,NULL,drw);CHKERRQ(ierr);}

  PetscInt lge = SSDCGetOptInt(NULL,"-lg_error",SSDCHasOptName(NULL,"-lg_error"));
  if (lge) {ierr = SSDCSetTSMonitorLGError(ssdc,ts,solution,NULL,lge);CHKERRQ(ierr);}

  PetscInt lgu = SSDCGetOptInt(NULL,"-lg_primary",SSDCHasOptName(NULL,"-lg_primary"));
  if (lgu) {ierr = SSDCSetTSMonitorLGIntegral(ssdc,ts,dof,evalU,params,lgu);CHKERRQ(ierr);}

  PetscInt lgs = SSDCGetOptInt(NULL,"-lg_entropy",SSDCHasOptName(NULL,"-lg_entropy"));
  if (lgs) {ierr = SSDCSetTSMonitorLGIntegral(ssdc,ts,1,evalS,params,lgs);CHKERRQ(ierr);}


  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  {
    PetscReal t; Vec U;
    ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
    ierr = SSDCCreateVec(ssdc,&U);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,NULL,t,U);CHKERRQ(ierr);
    ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
    ierr = VecDestroy(&U);CHKERRQ(ierr);
  }
  ierr = TSSetUp(ts);CHKERRQ(ierr);

  ierr = PetscLogStagePush(SolveStage);CHKERRQ(ierr);
  ierr = TSSolve(ts,NULL);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

#if 0
  PetscBool error = SSDCGetOptBool(NULL,"-error",PETSC_TRUE);
  if (error) {
    int i,deg;
    PetscReal t; Vec U,Ue;
    PetscReal errorL1[dof],errorL2[dof],errorMax[dof];
    ierr = TSGetSolveTime(ts,&t);CHKERRQ(ierr);
    ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
    ierr = SSDCGetWorkVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCFormSolution(ssdc,solution,NULL,t,Ue);CHKERRQ(ierr);
    ierr = SSDCComputeError(ssdc,U,Ue,NULL,errorL1,errorL2,errorMax);CHKERRQ(ierr);
    ierr = SSDCRestoreVec(ssdc,&Ue);CHKERRQ(ierr);
    ierr = SSDCGetDeg(ssdc,&deg);CHKERRQ(ierr);
    for (i=0; i<dof; i++) {
      ierr = PetscPrintf(comm,"Error L1: %.5e L2: %.5e Max: %.5e [ dim=%D deg=%D ]\n",
                         (double)errorL1[i],(double)errorL2[i],
                         (double)errorMax[i],dim,deg);CHKERRQ(ierr);
    }
  }
#endif

  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = SSDCDestroy(&ssdc);CHKERRQ(ierr);

  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
