    pure subroutine CharacteristicDecompEU3D(vl,vr,Re,ev,Jx)
    ! this subroutine calculates the left and right eigenvector
    ! matrices and the eigenvalues of the flux jacobian
    ! in the normal direction. The resulting right eigenvectors
    ! have the special magic property that R R^T = du/dw.
    ! input primitive variables
    real(wp), dimension(nq), intent(in) :: vl,vr
    ! eigenvalues of df/du
    real(wp), dimension(nq), intent(out) :: ev
    ! left and right eigenvectors of df/du, respectively.
    real(wp), dimension(nq,nq), intent(out) :: Re
   ! real(wp), dimension(nq,nq):: Le
    ! normal vector
    real(wp), intent(in) :: Jx(dim)

    real(wp), dimension(nq) :: vhat
    real(wp), dimension(6) :: ave             ! 
    real(wp) :: a_hat,h_hat,rho_hat, ltmp, ltmpi, cav, cav2, un
    real(wp) :: sqrt2,rhoinvg, tinvrho, invr, invrho, rhoinvg, rhoinvr           
    real(wp) :: sqgm1og, reorho, rhoot, invsqg, sqrot              ! 

   ! real(wp) :: z1_ave, z3_ave                        ! Logarithmic averages of density and temperature

   ! real(wp) ::  P1, P2                     ! 

    continue


    call average_roe2000s3d(vl,vr,ave)
    rho_hat      = ave(1)
    v_hat(1:3)   = ave(2:4)
    h_hat        = ave(5)
    t_hat        = ave(6)
    

    ltmp  = sqrt(dot_product(Jx,Jx))
    ltmpi = one/ltmp

    cav2 = gamma0*Ru0*t_hat
    cav  = sqrt(cav2)

    un = dot_product(v_hat(2:4),Jx)

    sqrt2    = sqrt(2.0_wp)/2.0_wp
    rhoinvg  = sqrt(rho_hat)/sqrt(gamma0)
    tinvrho  = sqrt(t_hat)/sqrt(rho_hat)
    invr     = 1.0_wp/sqrt(Ru0) 
    invrho   = 1.0_wp/sqrt(rho_hat) 
    rhoinvr  = sqrt(rho_hat)/sqrt(Ru0)
    sqgm1og  = sqrt(gm1og)
    
    reorho  = sqrt(Ru0/rho_hat)
    rhoot   = sqrt(rho_hat/t_hat)
    invsqg  = 1.0_wp/sqrt(gamma0)
    sqrot   = sqrt(Re0)/t_hat
 
    ev(1)   = abs(un - cav*ltmp)
    ev(2)   = abs(un + cav*ltmp) 
    ev(3:4) = abs(un) 

    ! start by calculating the right eigenvectors
    ! of the primtive flux jacobian dv/du*df/du*du/dv
    Re(:,:) = 0.0_wp;


    Re(1,1)  =  sqrt2*rhoinvg*invr
    Re(2,1)  = -sqrt2*tinvrho*Jx(1)*ltmpi
    Re(3,1)  = -sqrt2*tinvrho*Jx(2)*ltmpi
    Re(4,1)  = -sqrt2*tinvrho*Jx(3)*ltmpi
    Re(5,1)  =  sqrt2*gm1*t_hat*invr*invrho*invsqg

    Re(1,2)  = sqrt2*rhoinvg*invr
    Re(2,2)  = sqrt2*tinvrho*Jx(1)*ltmpi
    Re(3,2)  = sqrt2*tinvrho*Jx(2)*ltmpi
    Re(4,2)  = sqrt2*tinvrho*Jx(3)*ltmpi
    Re(5,2)  = sqrt2*gm1*t_hat*invr/sqrt(gamma0*rho_hat)

    Re(1,3)  = -sqgm1og*rhoinvr*Jx(1)*ltmpi
    Re(3,3)  = -tinvrho*Jx(3)*ltmpi
    Re(4,3)  =  tinvrho*Jx(2)*ltmpi
    Re(5,3)  =  sqgm1og*t_hat*invr*Jx(1)*ltmpi/sqrt(rho_hat)
     
    
    Re(1,4)  = -sqgm1og*rhoinvr*Jx(2)*ltmpi
    Re(2,4)  =  tinvrho*Jx(3)*ltmpi
    Re(4,4)  = -tinvrho*Jx(1)*ltmpi
    Re(5,4)  =  sqgm1og*t_hat*invr*Jx(2)*ltmpi/sqrt(rho_hat)

    Re(1,5)  = -sqgm1og*rhoinvr*Jx(3)*ltmpi
    Re(2,5)  = -tinvrho*Jx(2)*ltmpi
    Re(3,5)  =  tinvrho*Jx(1)*ltmpi
    Re(5,5)  =  sqgm1og*t_hat*invr*Jx(3)*ltmpi/sqrt(rho_hat)

    mattmp = dUdV(Vav)
    Re = matmul(mattmp,Re)


    Le(:,:) = huge(0.0_wp)  ! XXX Le not used!
    return  

    Le(1,1) =  sqrt2*reorho*invsqg
    Le(2,1) =  sqrt2*reorho*invsqg
    Le(3,1) = -sqgm1og*reorho*Jx(1)*ltmpi
    Le(4,1) = -sqgm1og*reorho*Jx(2)*ltmpi
    Le(5,1) = -sqgm1og*reorho*Jx(3)*ltmpi

    ! Second Column
    Le(1,2) = -sqrt2*rhoot*Jx(1)*ltmpi
    Le(2,2) =  sqrt2*rhoot*Jx(1)*ltmpi
    Le(4,2) =  rhoot*Jx(3)*ltmpi
    Le(5,2) = -rhoot*Jx(2)*ltmpi

    ! Third Column
    Le(1,3) = -sqrt2*rhoot*Jx(2)*ltmpi
    Le(2,3) =  sqrt2*rhoot*Jx(2)*ltmpi
    Le(3,3) = -rhoot*Jx(3)*ltmpi
    Le(5,3) =  rhoot*Jx(1)*ltmpi

    ! Fourth Column
    Le(1,4) = -sqrt2*rhoot*Jx(3)*ltmpi
    Le(2,4) =  sqrt2*rhoot*Jx(3)*ltmpi
    Le(3,4) =  rhoot*Jx(2)*ltmpi
    Le(4,4) = -rhoot*Jx(1)*ltmpi

    ! Fifth Column
    Le(1,5) =  sqrt2*rhoinvg*sqrot
    Le(2,5) =  sqrt2*rhoinvg*sqrot
    Le(3,5) =  rhoinvg*sqrot*Jx(1)*ltmpi/sqrt(gm1)
    Le(4,5) =  rhoinvg*sqrot*Jx(2)*ltmpi/sqrt(gm1)
    Le(5,5) =  rhoinvg*sqrot*Jx(3)*ltmpi/sqrt(gm1)


    mattmp = dVdU(Vav)
    Le     = matmul(Le,mattmp)

  end subroutine CharacteristicDecompEU

!===============================================================================================

 pure subroutine average_roe2000s(vl,vr,av)


    implicit none

    ! Arguments
    ! =========
    real(wp), intent(in), dimension(5)     :: vl, vr   ! left and right states
    ! Local Variables
    ! ===============

    real(wp), dimension(5) :: vhat             ! temporary variables
    real(wp), dimension(6) :: av 

    real(wp) :: root_Tl  , root_Tr             !   sqrt[T_i], i=L,R
    real(wp) :: root_Tl_I, root_Tr_I           ! 1/sqrt[T_i], i=L,R

    real(wp) :: tinvav, tinvavinv              ! average of inverse temperature and its inverse

    real(wp) :: s1, s2                         ! Logarithmic averages of density and temperature

    real(wp) :: mdot, P, T                     ! normal mass flux (mdot), Pressure,  Temperature

    continue

    root_Tl   = sqrt(vl(5))    ; root_Tr   = sqrt(vr(5))            ! Sqrt[T_i]
    root_Tl_I = 1.0_wp/root_Tl ; root_Tr_I = 1.0_wp/root_Tr         ! Sqrt[T_i]^{-1}
    tinvav    = root_Tl_I   + root_Tr_I
    tinvavinv = 1.0_wp/tinvav

    vhat(2:4) = (vl(2:4)*root_Tl_I + vr(2:4)*root_Tr_I)*tinvavinv   ! velocity

    P = (vl(1)*root_Tl + vr(1)*root_Tr) * tinvavinv                 ! pressure

    s1 = Logarithmic_Average(root_Tl*vl(1),root_Tr*vr(1))           ! Logarithmic average of rho/Sqrt(T)

    s2 = Logarithmic_Average(root_Tl_I    ,root_Tr_I    )           ! Logarithmic average of   1/Sqrt(T)

    vhat(1) = 0.5_wp *tinvav * s1                                   ! density

    T = 0.5_wp * (gm1og * P + gp1og * s1/s2) / vhat(1)              ! temperature

    vhat(5) = T + 0.5_wp * gm1M2 * dot_product(vhat(2:4),vhat(2:4)) ! total enthalpy


    av(1)   = vhat(1)
    av(2:4) = vhat(2:4)
    av(5)   = T
    av(6)   = vhat(5)


end subroutine average_roe2000s