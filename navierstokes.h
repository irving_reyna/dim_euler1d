#ifndef NAVIERSTOKES_H
#define NAVIERSTOKES_H

#if !defined(DIM)
#warning "DIM not defined, using DIM=3"
#define DIM 3
#endif

#if (DIM < 1) || (DIM > 3)
#error "DIM not in {1,2,3}"
#endif

#if DIM == 3
#include "navierstokes3d.h"
#define NS_PREFIX(symbol) ns3d_##symbol
#endif
#if DIM == 1
#include "navierstokes1d.h"
#define NS_PREFIX(symbol) ns1d_##symbol
#endif

#define ns_params        NS_PREFIX(params)
#define ns_setup         NS_PREFIX(setup)

#define ns_primitive     NS_PREFIX(primitive)
#define ns_conservative  NS_PREFIX(conservative)
#define ns_entropy       NS_PREFIX(entropy)
#define ns_primary       NS_PREFIX(primary)

#define ns_grad_w2v      NS_PREFIX(grad_w2v)
#define ns_grad_v2w      NS_PREFIX(grad_v2w)
#define ns_grad_w2u      NS_PREFIX(grad_w2v)
#define ns_grad_u2w      NS_PREFIX(grad_v2w)

#define ns_iflux         NS_PREFIX(iflux)
#define ns_iflux1n       NS_PREFIX(iflux1n)
#define ns_iflux2n       NS_PREFIX(iflux2n)
#define ns_upwind        NS_PREFIX(upwind)

#define ns_vflux         NS_PREFIX(vflux)
#define ns_vfluxn        NS_PREFIX(vfluxn)

#endif

