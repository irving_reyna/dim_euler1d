#ifndef NAVIERSTOKES1D_H
#define NAVIERSTOKES1D_H

#include <ssdc.h>

typedef struct {
  ssdc_real_t Mach0;
  ssdc_real_t gamma0;
  ssdc_real_t Pr0;
  ssdc_real_t Sfix;
  ssdc_real_t Ru0;
} ns1d_params;

extern void ns1d_setup(const ns1d_params *params);

extern void ns1d_primitive(const void *ctx,
                           const ssdc_real_t *U,
                           /* */ ssdc_real_t *V);

extern void ns1d_conservative(const void *ctx,
                              const ssdc_real_t *V,
                              /* */ ssdc_real_t *U);

extern void ns1d_entropy(const void *ctx,
                         const ssdc_real_t *U,
                         /* */ ssdc_real_t *W);

extern void ns1d_primary(const void *ctx,
                         const ssdc_real_t *W,
                         /* */ ssdc_real_t *U);

extern void ns1d_grad_w2u(const void *ctx,
                          const ssdc_real_t *U,
                          const ssdc_real_t *dWdX,
                          /* */ ssdc_real_t *dUdX);

extern void ns1d_grad_u2w(const void *ctx,
                          const ssdc_real_t *U,
                          const ssdc_real_t *dUdX,
                          /* */ ssdc_real_t *dWdX);

extern void ns1d_grad_w2v(const void *ctx,
                          const ssdc_real_t *U,
                          const ssdc_real_t *dWdX,
                          /* */ ssdc_real_t *dVdX);

extern void ns1d_grad_v2w(const void *ctx,
                          const ssdc_real_t *U,
                          const ssdc_real_t *dVdX,
                          /* */ ssdc_real_t *dWdX);

extern void ns1d_iflux(const void *ctx,
                       const ssdc_real_t *U,
                       /* */ ssdc_real_t *F);

extern void ns1d_iflux1n(const void *ctx,
                         const ssdc_real_t *U,
                         const ssdc_real_t *Sn,
                         /* */ ssdc_real_t *Fn);

extern void ns1d_iflux2n(const void *ctx,
                         const ssdc_real_t *Um,
                         const ssdc_real_t *Up,
                         const ssdc_real_t *Sn,
                         /* */ ssdc_real_t *Fn);

extern void ns1d_upwind(const void *ctx,
                        const ssdc_real_t *Um,
                        const ssdc_real_t *Up,
                        const ssdc_real_t *Sn,
                        /* */ ssdc_real_t *vec);

extern void ns1d_vflux(const void *ctx,
                       const ssdc_real_t *U,
                       const ssdc_real_t *G,
                       /* */ ssdc_real_t *F);

extern void ns1d_vfluxn(const void *ctx,
                        const ssdc_real_t *U,
                        const ssdc_real_t *G,
                        const ssdc_real_t *Sn,
                        /* */ ssdc_real_t *Fn);

#endif
