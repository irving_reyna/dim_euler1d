module navierstokes1d
  use iso_c_binding, only: C_PTR
  use iso_c_binding, only: C_DOUBLE
  implicit none

  private
  public ns1d_setup
  public ns1d_primitive
  public ns1d_conservative
  public ns1d_entropy
  public ns1d_primary
  public ns1d_dWdV
  public ns1d_dVdW
  public ns1d_grad_w2u
  public ns1d_grad_u2w
  public ns1d_grad_w2v
  public ns1d_grad_v2w
  public ns1d_iflux1n
  public ns1d_iflux2n
  public ns1d_upwind
  public ns1d_vflux
  public ns1d_vfluxn

  integer, parameter :: rk  = C_DOUBLE
  integer, parameter :: dim = 1
  integer, parameter :: dof = dim+2

  type, bind(C) :: Params
     real(kind=rk) :: Mach0
     real(kind=rk) :: gamma0
     real(kind=rk) :: Pr0
     real(kind=rk) :: Sfix
     real(kind=rk) :: Ru0
  end type Params

  integer, parameter :: wp  = rk
  integer, parameter :: nd  = dim
  integer, parameter :: nq  = dof

  real(wp), parameter :: zero     = 0.0_wp
  real(wp), parameter :: one      = 1.0_wp
  real(wp), parameter :: two      = 2.0_wp
  real(wp), parameter :: three    = 3.0_wp
  real(wp), parameter :: four     = 4.0_wp
  real(wp), parameter :: five     = 5.0_wp
  real(wp), parameter :: six      = 6.0_wp
  real(wp), parameter :: seven    = 7.0_wp
  real(wp), parameter :: eight    = 8.0_wp
  real(wp), parameter :: nine     = 9.0_wp
  real(wp), parameter :: ten      = 10._wp
  real(wp), parameter :: half     = 0.5_wp
  real(wp), parameter :: third    = 1.0_wp/3.0_wp
  real(wp), parameter :: twothird = 2.0_wp/3.0_wp
  real(wp), parameter :: fourth   = 1.0_wp/4.0_wp
  real(wp), parameter :: fifth    = 1.0_wp/5.0_wp
  real(wp), parameter :: sixth    = 1.0_wp/6.0_wp
  real(wp), parameter :: seventh  = 1.0_wp/7.0_wp
  real(wp), parameter :: eigth    = 1.0_wp/8.0_wp
  real(wp), parameter :: ninth    = 1.0_wp/9.0_wp
  real(wp), parameter :: tenth    = 1.0_wp/10._wp
  real(wp), parameter :: small    = 1.0e-10_wp
  real(wp), parameter :: large    = 1.0e05_wp
  real(wp), parameter :: realsmall = 1.e-20_wp, big = 1.e10_wp, realbig = 1.e20_wp
  real(wp), parameter :: pi = 3.1415926535897932384626433832795028841971693993751058209749_wp

  ! Universal gas constant
  !real(wp), parameter :: Ru = 8314.472_wp ! (J/kmol-K)
  ! Reference temperature for enthalpy calculation
  real(wp), parameter :: Tref = 298.15_wp ! (K)
  ! Atmospheric Pressure
  real(wp), parameter :: Patm = 101325.0_wp ! (Pa)

  real(wp) :: gamma0  =    1.4_wp
  real(wp) :: Mach0  !=    0.08_wp
  real(wp) :: Ru0     =    1.0_wp
  real(wp) :: Pr0    !=    0.72_wp
  real(wp) :: Sfix    = 4.95e-6_wp

  real(wp) :: Cp      
  real(wp) :: Cv      
  real(wp) :: gamI   != 1/gamma0
  real(wp) :: gm1I   != 1/gamma0
  real(wp) :: gm1    != gamma0 - 1
  real(wp) :: gp1    != gamma0 + 1
  real(wp) :: gm1og  != gm1/gamma0
  real(wp) :: gp1og  != gp1/gamma0


contains


!===============================================================================================================
!from primitive variables to conserved variables

  pure subroutine primitive_to_conserved(vin,uout)
    ! primitive variables
    real(wp), intent(in) :: vin(nq)
    ! conserved variables
    real(wp), intent(out) :: uout(nq)

    ! density
    uout(1) = vin(1)
    ! momentum
    uout(2) = vin(1)*vin(2)
    ! energy
    !cv = R/gama-1
    !uout(3) = vin(1)*(  Ru0*vin(3)/(gamma0-1.0_wp) + 0.5_wp*vin(2)*vin(2) )
    uout(3) = vin(1)*(  Cv*vin(3) + 0.5_wp*vin(2)*vin(2) )


  end subroutine primitive_to_conserved


!=================================================================================================================
!from conserved variables to primitive variables

    pure subroutine conserved_to_primitive(uin,vout)
    ! conserved variables
    real(wp), intent(in) :: uin(nq)
    ! primitive variables
    real(wp), intent(out) :: vout(nq)
    ! density
    vout(1) = uin(1)
    ! velocity
    vout(2) = uin(2)/uin(1)
    ! PRESSURE
    vout(3) = ( uin(3)/uin(1) - 0.5_wp*vout(2)*vout(2) )/Cv
    !vout(3) = ( uin(3)/uin(1) - 0.5_wp*vout(2)*vout(2) )*(gamma0-1.0_wp)/Ru0

  end subroutine conserved_to_primitive

!==========================================================================================================

  pure function specificentropy(vin)
    ! primitive variables
    real(wp), intent(in) :: vin(nq)
    ! output thermodynamic specific entropy
    real(wp) :: specificentropy

    specificentropy = (Ru0/gm1)*log(vin(3)) - Ru0*log(vin(1))

  end function specificentropy

!================================================================================================================
!from conserved variables to entropy variables
  pure subroutine conserved_to_entropy(uin,wout)
    ! conserved variables
    real(wp), intent(in) :: uin(nq)
    ! entropy variables
    real(wp), intent(out) :: wout(nq)
    ! primitive variables
    real(wp)              :: vtmp(nq)

    ! density
    vtmp(1) = uin(1)
    ! velocity
    vtmp(2) = uin(2)/uin(1)
    ! PRESSURE
   ! vtmp(3) = ( uin(3)/uin(1) - 0.5_wp*vtmp(2)*vtmp(2) )*(gamma0-1.0_wp)/Ru0
    vtmp(3) = ( uin(3)/uin(1) - 0.5_wp*vtmp(2)*vtmp(2) )/Cv

    !wout(1) = gamma0*Ru0/gm1 - specificentropy(vtmp) -0.5_wp*vtmp(2)*vtmp(2)/vtmp(3)
    wout(1) = Cp - specificentropy(vtmp) -0.5_wp*vtmp(2)*vtmp(2)/vtmp(3)

    wout(2) = vtmp(2)/vtmp(3)
    ! w_5 = -1/T
    wout(3) = -1.0_wp/vtmp(3)

  end subroutine conserved_to_entropy

!===================================================================================================================
!from entropy variables to primitive variables

  pure subroutine entropy_to_primitive(win,vout)
    implicit none
    ! primitive variables
    real(wp), intent(in) :: win(nq)
    ! entropy variables
    real(wp), intent(out) :: vout(nq)

    vout(3) = -1.0_wp/win(3)


    vout(2) = -win(2)/win(3) 

    ! Density cp = gama*R/(gama-1)
    !vout(1) = exp(  ( win(1) -gamma0*Ru0/gm1 + (Ru0/gm1)*log(-1.0_wp/win(3)) + &
     !           0.5_wp*(-win(2)*win(2)/win(3)) )/Ru0   )
    vout(1) = exp(  ( win(1) -Cp + Cv*log(-1.0_wp/win(3)) + &
                0.5_wp*(-win(2)*win(2)/win(3)) )/Ru0   )

  end subroutine entropy_to_primitive
!=====================================================================================================================

  pure function dUdV(Vin)
    ! Checked MHC 08_09_13
    ! this function calculates the jacobian of the
    ! conserved variables with respect to the primitive
    ! variables
    ! primitive variables
    real(wp), intent(in) :: Vin(nq)

    ! output jacobian
    real(wp) :: dUdV(nq,nq)

    dUdV = 0.0_wp

    ! continuity
    dUdV(1,1)  = +1.0_wp
    ! momentum
    dUdV(2,1)  = vin(2)
    dUdV(2,2)  = vin(1)
    
    dUdV(3,1)  = Ru0*vin(3)/gm1 + 0.5*vin(2)*vin(2)
    dUdV(3,2)  = vin(1)*vin(2)
    dUdV(3,3)  = vin(1)*Ru0/gm1

  end function dUdV
!=========================================================================================================================


  pure function dVdU(Vin)
    ! Checked MHC 08_09_13
    ! this function calculates the jacobian of the
    ! primitive variables with respect to the conserved
    ! variables
    ! primitive variables
    real(wp), intent(in) :: Vin(nq)

    ! output jacobian
    real(wp) :: dVdU(nq,nq)

    real(wp) :: rhoinv, cvinv,u3, u2

    cvinv  = gm1/Ru0
    !u3     = vin(1)*( (Ru0/gm1)*vin(3) + 0.5_wp*vin(2)*vin(2) )
    u3     = vin(1)*( Cv*vin(3) + 0.5_wp*vin(2)*vin(2) )
    u2     = vin(1)*vin(2)
    rhoinv = 1.0_wp/vin(1)

    dVdU = 0.0_wp

    dVdU(1,1)   = +1.0_wp

    dVdU(2,1)   = -vin(2)*rhoinv
    dVdU(2,2)   = +rhoinv
    
    dVdU(3,1)   =  cvinv*rhoinv*rhoinv*(-u3 + u2*u2*rhoinv)
    dVdU(3,2)   =  -u2*cvinv*rhoinv*rhoinv
    dVdU(3,3)   =  cvinv*rhoinv


  end function dVdU

!=============================================================================================================================

  pure function dWdV(Vin)
    ! Checked MHC 08_09_13
    ! this function calculates the jacobian of the
    ! primitive variables with respect to the Entropy variables.
    ! primitive variables
    real(wp), intent(in) :: Vin(nq)

    ! output jacobian
    real(wp) :: dWdV(nq,nq)

    real(wp) :: tinv,t2inv

    tinv  = 1.0_wp / vin(3)
    t2inv = tinv*tinv


    dWdV  = 0.0_wp

    dWdV(1,1) =  Ru0/vin(1)
    dWdV(1,2) = -vin(2)*tinv
    dWdV(1,3) = (-Ru0/gm1)*tinv + 0.5*vin(2)*vin(2)*t2inv

    dWdV(2,2) =  tinv
    dWdV(2,3) = -vin(2)*t2inv

    dWdV(3,3) =  t2inv

  end function dWdV

!==============================================================================================================================

  pure function dVdW(Vin)
    ! Checked MHC 08_09_13
    ! this function calculates the jacobian of the
    ! primitive variables with respect to the Entropy variables.
    ! number of equations
    ! primitive variables
    real(wp), intent(in) :: Vin(nq)

    ! output jacobian
    real(wp) :: dVdW(nq,nq)

    real(wp) :: s,w1,w2,w3,exp_arg, invw3, inv2w3, invre

    s  = specificentropy(vin)

    !w1 = gamma0*Ru0/gm1 - specificentropy(vin) -0.5_wp*vin(2)*vin(2)/vin(3)
    w1 = Cp - specificentropy(vin) -0.5_wp*vin(2)*vin(2)/vin(3)
    w2 = vin(2)/vin(3)
    w3 = -1.0_wp/vin(3)
    
    invw3  = 1.0_wp/w3
    inv2w3 = invw3*invw3 
    invre  = 1.0_wp/Ru0

   ! exp_arg = (-gamma0*Ru0/gm1 +w1 -0.5*w2*w2/w3 + (Ru0/gm1)*log(-1.0/w3) )*invre
    !exp_arg = exp( (-Cp +w1 -0.5*w2*w2/w3 + Cv*log(-1.0/w3) )*invre )
    exp_arg = vin(1)

    dVdW = 0.0_wp

    ! first calculate dv/dW
    dVdW(1,1) = exp_arg*invre
    dVdW(1,2) = exp_arg*( -w2*invw3*invre )
    dVdW(1,3) = exp_arg*( 0.5*w2*w2*invre*inv2w3 + (-invw3/gm1) )

    dVdW(2,2) = -invw3
    dVdW(2,3) = w2*inv2w3

    dVdW(3,3) = inv2w3

  end function dVdW

!========================================================================================================================

  pure function dWdU(Vin)
    ! Checked 11-1-2013
    ! this function calculates the jacobian of the
    ! entropy variables with respect to the conserved
    ! variables using the chain rule, dw/du = dw/dv*dv/du.
    ! primitive variables
    real(wp), intent(in) :: Vin(nq)

    ! output jacobian
    real(wp) :: dWdU(nq,nq)

    real(wp) :: W_V(nq,nq), V_U(nq,nq)

    W_V  = dWdV(Vin)
    V_U  = dVdU(Vin)
    dWdU = matmul(W_V,V_U)

  end function dWdU

!=========================================================================================================================

  pure function dUdW(Vin)
    ! this function calculates the jacobian of the
    ! conserved variables with respect to the entropy
    ! variables using the chain rule, du/dw = du/dv*dv/dw.

    ! primitive variables
    real(wp), intent(in) :: Vin(nq)

    ! output jacobian
    real(wp) :: dUdW(nq,nq)

    real(wp) :: U_V(nq,nq), V_W(nq,nq)

    U_V  = dUdV(Vin)
    V_W  = dVdW(Vin)
    dUdW = matmul(U_V,V_W)

  end function dUdW


!==========================================================================================================================
!Euler fluxes


  pure function NormalFlux(vin,nx)
    ! this function calculates the convective flux in the normal
    ! direction. Note that because we nondimensionalize the pressure
    ! by the reference pressure that there is a nondimensional parameter
    ! in the flux.
    ! normal vector
    real(wp), intent(in) :: nx(dim)
    ! primitive variables
    real(wp), intent(in) :: vin(nq)

    ! output normal flux
    real(wp) :: normalflux(nq)

    ! local variables
    !
    ! normal mass flux
    real(wp) :: un,p
    ! pressure
    !real(wp) :: p

    ! calculate normal mass flux
    un = vin(1)*vin(2)*nx(1)
     p = Ru0*vin(1)*vin(3)

    ! mass flux (\rho u \cdot n)
    normalflux(1) = vin(1)*vin(2)*nx(1)
    ! momentum flux (\rho u \cdot n) u + p n /(gamma_0 M_0^2)
    normalflux(2) = vin(1)*vin(2)*vin(2)*nx(1) + p*nx(1)

    ! energy flux (\rho u \cdot n) H
   !normalflux(3) = un*(  Ru0*gamma0*vin(3)/gm1  + 0.5_wp*vin(2)*vin(2) )
    normalflux(3) = un*(  Cp*vin(3)  + 0.5_wp*vin(2)*vin(2) )

   !  normalflux(3) = un*( vin(3)/((gamma0)*vin(1)) &
    !  + 0.5_wp*(vin(2)*vin(2))  )

  end function NormalFlux

!=====================================================================================================================

 pure function ViscousFlux(vin,phi)

         implicit none

          real(wp), dimension(nq,dim) :: ViscousFlux
         ! left and right states\
         real(wp), intent(in)       :: vin(nq)
    ! entropy variable gradients
         real(wp), intent(in)       :: phi(nq,dim)
         real(wp), dimension(nq)    :: fvl,gradv, phi_int
         real(wp), dimension(nq,nq) :: mat_dvdw
         real(wp)                   :: kappa, mu, invrho, inv2rho
         
         phi_int(:) = phi(:,dim)

         mu       =  sutherland_law(vin(3))
         kappa    =  1.0_wp/Pr0!2.0_wp*gamma0*Ru0*4.0_wp*mu/(3.0_wp*gm1)
         
         invrho   = 1.0_wp/vin(1)
         inv2rho  = invrho*invrho

         mat_dvdw = dVdW(vin)
         gradv    = matmul(mat_dvdw, phi_int)  

         ! local variables
         fvl(1) = 0.0_wp
         fvl(2) = 4.0_wp*mu*( gradv(2) )/3.0_wp
         fvl(3) = 4.0_wp*mu*vin(2)*( gradv(2) )/3.0_wp + kappa*(gradv(3))

         !fvl(1) = 0.0_wp
         !fvl(2) = (4.0_wp*mu*vin(3)*(phi(2,1) + vin(2)*phi(3,1)))/(3.0_wp)
         !fvl(3) = (vin(3)*(3.0_wp*vin(3)*kappa*phi(3,1) + 4.0_wp*mu*vin(2)*(phi(2,1)  &
         !       + vin(2)*phi(3,1))))/(3.0_wp)

         ViscousFlux(:,dim) = fvl(:) !/ Ru0

         !ViscousFlux(:,dim) = fvl(:)

       end function ViscousFlux

! ===============================================================================================================

!fEntropy consistent flux
  pure function EntropyConsistentFlux(vl,vr,Jx)

    ! this function calculates the normal entropy consistent
    ! flux based on left and right states of primitive variables.
    ! it is consistent with the nondimensionalization employed
    ! herein and follows directly from the work of Ismail and Roe,
    ! DOI: 10.1016/j.jcp.2009.04.021

    !use nsereferencevariables, only: gM2I, gm1og, gp1og, gm1M2

    implicit none
    ! Arguments
    ! =========
    real(wp), intent(in), dimension(nq)     :: vl, vr   ! left and right states
    real(wp), intent(in), dimension(dim)     :: Jx       ! metrics scaled by jacobian

    ! Function
    ! ========
    real(wp), dimension(nq) :: EntropyConsistentFlux
    ! Local Variables
    ! ===============
    real(wp), dimension(nq) :: vhat             ! temporary variables


    real(wp) :: root_l, root_r          ! 1/sqrt[T_i], i=L,R
    real(wp) :: tinvav, tinvavinv              ! average of inverse temperature and its inverse
    real(wp) :: z3_ave, z1_ave                        ! Logarithmic averages of density and temperature
    real(wp) :: mdot, P1, P2,T                     ! normal mass flux (mdot), Pressure,  Temperature

    continue

    root_l   = sqrt( 1.0_wp/( Ru0*vl(3)) )  ; root_r  = sqrt( 1.0_wp/( Ru0*vr(3)) )            ! Sqrt[T_i]
        ! Sqrt[T_i]^{-1}

    tinvav    = root_l  + root_r
    tinvavinv = 1.0_wp/tinvav

    vhat(2) = ( vl(2)*root_l + vr(2)*root_r )*tinvavinv   ! velocity

    P1      = ( vl(1)/root_l + vr(1)/root_r )*tinvavinv  ! pressure, p_1 in (Ismail and Roe)

    z3_ave  = Logarithmic_Average( vl(1)/root_l,vr(1)/root_r )  !z3 log ave          ! Logarithmic average of rho/Sqrt(T)

    z1_ave  = Logarithmic_Average( root_l    ,root_r    )       !z1 log ave       ! Logarithmic average of   1/Sqrt(T)

    vhat(1) = 0.5_wp*tinvav*z3_ave                              ! density

    P2      = 0.5_wp*( gm1og*P1 + gp1og*z3_ave/z1_ave )   ! Now it is P_2              ! temperature

    T       = P2/(vhat(1)*Ru0)

   !vhat(3) = gamma0*Ru0*T/gm1  + 0.5_wp*( vhat(2)*vhat(2) ) ! total enthalpy
    vhat(3) = Cp*T  + 0.5_wp*( vhat(2)*vhat(2) ) ! total enthalpy
 
    mdot    = vhat(1)*(vhat(2)*Jx(1))                        ! normal mass flow rate

    EntropyConsistentFlux(1) = mdot
    EntropyConsistentFlux(2) = mdot*vhat(2) + Jx(1)*P1 
    EntropyConsistentFlux(3) = mdot*vhat(3)

  end function EntropyConsistentFlux


!======================================================================================================================
!Average states

  pure function Logarithmic_Average(a,b)

!   use variables, only: Log_Ave_Counter

    implicit none

    real(wp),  intent(in) :: a,b

!  Logarithmic expansion valid to us = 0.00000894, or ratio =  1.006
!   real(wp), dimension(0:1), parameter :: c = (/1.0_wp,3.0_wp/5.0_wp/)
!   real(wp), dimension(0:1), parameter :: d = (/2.0_wp,8.0_wp/15.0_wp/)
!   real(wp),                 parameter :: eps = 8.9e-06_wp

!  Logarithmic expansion valid to us = 0.00276, or ratio =  1.111
!   real(wp), dimension(0:2), parameter :: c = (/1.0_wp,10.0_wp/9.0_wp,5.0_wp/21.0_wp /)
!   real(wp), dimension(0:2), parameter :: d = (/2.0_wp,14.0_wp/9.0_wp,128.0_wp/945.0_wp /)
!   real(wp),                 parameter :: eps = 2.7e-03_wp

!  Logarithmic expansion valid to us = 0.0226, or ratio =  1.353
!   real(wp), dimension(0:3), parameter :: c = (/1.0_wp,21.0_wp/13.0_wp,105.0_wp/143.0_wp,35.0_wp/429.0_wp /)
!   real(wp), dimension(0:3), parameter :: d = (/2.0_wp,100.0_wp/39.0_wp,566.0_wp/715.0_wp,512.0_wp/15015.0_wp /)
!   real(wp),                 parameter :: eps = 2.0e-02_wp

!  Logarithmic expansion valid to us = 0.0677, or ratio =  1.703
!   real(wp), dimension(0:4), parameter :: c = (/1.0_wp,36.0_wp/17.0_wp,126.0_wp/85.0_wp, &
!                                                84.0_wp/221.0_wp,63.0_wp/2431.0_wp/)
!   real(wp), dimension(0:4), parameter :: d = (/2.0_wp,182.0_wp/51.0_wp,166.0_wp/85.0_wp, &
!                                              2578.0_wp/7735.0_wp,32768.0_wp/3828825.0_wp/)
!   real(wp),                 parameter :: eps = 6.0e-02_wp

!  Logarithmic expansion valid to us = 0.135, or ratio =  2.165
    real(wp), dimension(0:5), parameter :: c = (/1.0_wp, 55.0_wp/21.0_wp, 330.0_wp/133.0_wp, &
                                        330.0_wp/323.0_wp, 55.0_wp/323.0_wp, 33.0_wp/4199.0_wp/)
    real(wp), dimension(0:5), parameter :: d = (/2.0_wp, 32.0_wp/7.0_wp, 3092.0_wp/855.0_wp, &
                        7808.0_wp/6783.0_wp, 17926.0_wp/142443.0_wp, 131072.0_wp/61108047.0_wp/)
    real(wp),                 parameter :: eps = 1.1e-01_wp

!  Logarithmic expansion valid to us = 0.221, or ratio =  2.776
!   real(wp), dimension(0:6), parameter :: c = (/1.0_wp,78.0_wp/25.0_wp,429.0_wp/115.0_wp,  &
!                                                1716.0_wp/805.0_wp, 1287.0_wp/2185.0_wp,   &
!                                                2574.0_wp/37145.0_wp, 429.0_wp/185725.0_wp/)
!   real(wp), dimension(0:6), parameter :: d = (/2.0_wp,418.0_wp/75.0_wp,3324.0_wp/575.0_wp,  &
!                                               55116.0_wp/20125.0_wp,399118.0_wp/688275.0_wp,&
!                                               1898954.0_wp/42902475.0_wp,                   &
!                                               2097152.0_wp/3904125225.0_wp/)
!   real(wp),                 parameter :: eps = 2.0e-01_wp

!  Logarithmic expansion valid to us = 0.315, or ratio =  3.560
!   real(wp), dimension(0:7), parameter :: c = (/1.0_wp,105.0_wp/29.0_wp,455.0_wp/87.0_wp,  &
!                                                1001.0_wp/261.0_wp,1001.0_wp/667.0_wp,    &
!                                                1001.0_wp/3335.0_wp,1001.0_wp/38019.0_wp, &
!                                                143.0_wp/215441.0_wp/)
!   real(wp), dimension(0:7), parameter :: d = (/2.0_wp,572.0_wp/87.0_wp,3674.0_wp/435.0_wp,  &
!                                                3256.0_wp/609.0_wp, 31054.0_wp/18009.0_wp,   &
!                                        86644.0_wp/330165.0_wp, 3622802.0_wp/244652265.0_wp, &
!                                                8388608.0_wp/62386327575.0_wp /)
!   real(wp),                 parameter :: eps = 3.0e-01_wp

    real(wp)              :: xi, gs, us, ave

    real(wp)              :: Logarithmic_Average

    xi  = a/b
    gs  = (xi-1.0_wp)/(xi+1.0_wp)
    us  = gs*gs
    ave = a + b

!   if(                        (us <= 1.0e-8_wp)) Log_Ave_Counter( 1) = Log_Ave_Counter( 1) + 1
!   if((1.0e-8_wp <= us) .and. (us <= 1.0e-7_wp)) Log_Ave_Counter( 2) = Log_Ave_Counter( 2) + 1
!   if((1.0e-7_wp <= us) .and. (us <= 1.0e-6_wp)) Log_Ave_Counter( 3) = Log_Ave_Counter( 3) + 1
!   if((1.0e-6_wp <= us) .and. (us <= 1.0e-5_wp)) Log_Ave_Counter( 4) = Log_Ave_Counter( 4) + 1
!   if((1.0e-5_wp <= us) .and. (us <= 1.0e-4_wp)) Log_Ave_Counter( 5) = Log_Ave_Counter( 5) + 1
!   if((1.0e-4_wp <= us) .and. (us <= 1.0e-3_wp)) Log_Ave_Counter( 6) = Log_Ave_Counter( 6) + 1
!   if((1.0e-3_wp <= us) .and. (us <= 1.0e-2_wp)) Log_Ave_Counter( 7) = Log_Ave_Counter( 7) + 1
!   if((1.0e-2_wp <= us) .and. (us <= 1.0e-1_wp)) Log_Ave_Counter( 8) = Log_Ave_Counter( 8) + 1
!   if((1.0e-1_wp <= us) .and. (us <= 1.0e-0_wp)) Log_Ave_Counter( 9) = Log_Ave_Counter( 9) + 1
!   if((      eps <= us)                        ) Log_Ave_Counter(10) = Log_Ave_Counter(10) + 1

    if(us <= eps) then
      Logarithmic_Average = &
!     ave * (c(0)-us*c(1)) / &
!           (d(0)-us*d(1))
!     ave * (c(0)-us*(c(1)-us*c(2))) / &
!           (d(0)-us*(d(1)-us*d(2)))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*c(3)))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*d(3))))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*c(4))))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*d(4)))))
      ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*(c(4)-us*c(5)))))) / &
            (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*(d(4)-us*d(5))))))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*(c(4)-us*(c(5)-us*c(6))))))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*(d(4)-us*(d(5)-us*d(6)))))))
!     ave * (c(0)-us*(c(1)-us*(c(2)-us*(c(3)-us*(c(4)-us*(c(5)-us*(c(6)-us*c(7)))))))) / &
!           (d(0)-us*(d(1)-us*(d(2)-us*(d(3)-us*(d(4)-us*(d(5)-us*(d(6)-us*d(7))))))))
    else
      Logarithmic_Average = ave * gs / log(xi)
    endif

  end function Logarithmic_Average

!====================================================================================================================
!Characteristic decomposition

    pure subroutine CharacteristicDecompEU(vl,vr,Re,ev,Jx)
    ! this subroutine calculates the left and right eigenvector
    ! matrices and the eigenvalues of the flux jacobian
    ! in the normal direction. The resulting right eigenvectors
    ! have the special magic property that R R^T = du/dw.
    ! input primitive variables
    real(wp), dimension(nq), intent(in) :: vl,vr
    ! eigenvalues of df/du
    real(wp), dimension(nq), intent(out) :: ev
    ! left and right eigenvectors of df/du, respectively.
    real(wp), dimension(nq,nq), intent(out) :: Re
   ! real(wp), dimension(nq,nq):: Le
    ! normal vector
    real(wp), intent(in) :: Jx(dim)

    real(wp), dimension(nq) :: vhat
    real(wp), dimension(4) :: ave             ! 
    real(wp) :: a_hat,u_hat,h_hat,rho_hat 
   ! real(wp) :: root_l, root_r           
   ! real(wp) :: tinvav, tinvavinv              ! 

   ! real(wp) :: z1_ave, z3_ave                        ! Logarithmic averages of density and temperature

   ! real(wp) ::  P1, P2                     ! 

    continue

  !  root_l   = sqrt(vl(1)/vl(3))    ; root_r  = sqrt(vr(1)/vr(3))    

  !  tinvav    = root_l  + root_r
  !  tinvavinv = 1.0_wp/tinvav

   ! vhat(2) = (vl(2)*root_l + vr(2)*root_r)*tinvavinv   ! velocity

   ! P1 = (vl(3)*root_l + vr(3)*root_r) * tinvavinv             ! pressure, p_1 in (Ismail and Roe)

   ! z3_ave = Logarithmic_Average(root_l*vl(3),root_r*vr(3))  !z3 log ave          ! Logarithmic average of rho/Sqrt(T)

   ! z1_ave = Logarithmic_Average(root_l    ,root_r    )       !z1 log ave       ! Logarithmic average of   1/Sqrt(T)

   ! vhat(1) = 0.5_wp*tinvav*z3_ave                                 ! rho_hat

   ! P2 = 0.5_wp * (gm1og * P1 + gp1og * z3_ave/z1_ave)   ! Now it is P_2_hat

   ! vhat(3) = gamma0*P2/(gm1*vhat(1))  + 0.5_wp*(vhat(2)*vhat(2)) ! total enthalpy
  
   ! u_hat = vhat(2)
   ! h_hat = vhat(3)
   ! a_hat = sqrt(gamma0*P2/vhat(1))  

    call average_roe2000s(vl,vr,ave)
    
    u_hat   = ave(1)
    h_hat   = ave(2)
    a_hat   = ave(3)
    rho_hat = ave(4)

    ! calculate eigenvalues
    ev(1)   = abs(u_hat - a_hat)*rho_hat*0.5_wp/gamma0
    ev(2)   = abs(u_hat)*gm1og*rho_hat 
    ev(3)   = abs(u_hat + a_hat)*rho_hat*0.5_wp/gamma0 

    ! start by calculating the right eigenvectors
    ! of the primtive flux jacobian dv/du*df/du*du/dv
    Re(:,:) = 0.0_wp;

    ! First Vector
    Re(1,1)  =  1.0_wp
    Re(2,1)  =  u_hat - a_hat
    Re(3,1)  =  h_hat - u_hat*a_hat

    ! Second Vector
    Re(1,2)  = 1.0_wp
    Re(2,2)  = u_hat
    Re(3,2)  = 0.5_wp*( u_hat*u_hat )

    ! Fifth Vector
    Re(1,3)  =  1.0_wp
    Re(2,3)  =  u_hat + a_hat
    Re(3,3)  =  h_hat + u_hat*a_hat


  end subroutine CharacteristicDecompEU

!====================================================================================================================

    pure subroutine CharacteristicDecompEURoe(vl,vr,Re,ev,Jx)
    ! this subroutine calculates the left and right eigenvector
    ! matrices and the eigenvalues of the flux jacobian
    ! in the normal direction. The resulting right eigenvectors
    ! have the special magic property that R R^T = du/dw.
    ! input primitive variables
    real(wp), dimension(nq), intent(in) :: vl,vr
    ! eigenvalues of df/du
    real(wp), dimension(nq), intent(out) :: ev
    ! left and right eigenvectors of df/du, respectively.
    real(wp), dimension(nq,nq), intent(out) :: Re
   ! real(wp), dimension(nq,nq):: Le
    ! normal vector
    real(wp), intent(in) :: Jx(dim)

    real(wp) :: u_hat, h_hat, a_hat, ave(nq)            ! 

   ! real(wp) :: root_l, root_r, suminv           
   ! real(wp) :: hr,hl              ! 
    continue

    !root_l   = sqrt(vl(1))    
    !root_r   = sqrt(vr(1))

    !suminv   = 1.0_wp/(root_r + root_l)
    
    !v_hat     = (root_l*vl(2) + root_r*vr(2))*suminv

    !hl = (vl(3)*gamma0)/(vl(1)*(gamma0-1.0_wp))  + 0.5_wp*vl(2)*vl(2) 
    !hr = (vr(3)*gamma0)/(vr(1)*(gamma0-1.0_wp))  + 0.5_wp*vr(2)*vr(2) 

    !h_hat   = (root_l*hl  + root_r*hr)*suminv
    !a_hat   = sqrt( (gamma0 -1.0_wp)*( h_hat -0.5_wp*v_hat*v_hat)  ) 

    call average_roe80s(vl,vr,ave)
    
    u_hat = ave(1)
    h_hat = ave(2)
    a_hat = ave(3)

    ! calculate eigenvalues
    ev(1)   = abs(u_hat - a_hat)
    ev(2)   = abs(u_hat) 
    ev(3)   = abs(u_hat + a_hat) 

    ! start by calculating the right eigenvectors
    ! of the primtive flux jacobian dv/du*df/du*du/dv
    Re(:,:) = 0.0_wp;

    ! First Vector
    Re(1,1)  =  1.0_wp
    Re(2,1)  =  u_hat - a_hat
    Re(3,1)  =  h_hat - u_hat*a_hat

    ! Second Vector
    Re(1,2)  = 1.0_wp
    Re(2,2)  = u_hat
    Re(3,2)  = 0.5_wp*( u_hat*u_hat )

    ! Fifth Vector
    Re(1,3)  =  1.0_wp
    Re(2,3)  = u_hat + a_hat
    Re(3,3)  = h_hat + u_hat*a_hat

  end subroutine CharacteristicDecompEURoe

!====================================================================================================================

  pure subroutine inverse(A,invA)

          real(wp), intent(in) :: A(dof,dof)
          real(wp), intent(out) :: invA(dof,dof)
          real(wp)  ::  detA

          detA = + A(1,1) * ( A(2,2)*A(3,3) - A(3,2)*A(2,3) ) &
          - A(2,1) * ( A(1,2)*A(3,3) - A(3,2)*A(1,3) ) &
          + A(3,1) * ( A(1,2)*A(2,3) - A(2,2)*A(1,3) )
          
          invA(1,1) = + A(2,2)*A(3,3) - A(2,3)*A(3,2) 
          invA(2,1) = - A(2,1)*A(3,3) + A(2,3)*A(3,1)
          invA(3,1) = + A(2,1)*A(3,2) - A(2,2)*A(3,1)
          invA(1,2) = - A(1,2)*A(3,3) + A(1,3)*A(3,2)
          invA(2,2) = + A(1,1)*A(3,3) - A(1,3)*A(3,1)
          invA(3,2) = - A(1,1)*A(3,2) + A(1,2)*A(3,1)
          invA(1,3) = + A(1,2)*A(2,3) - A(1,3)*A(2,2)
          invA(2,3) = - A(1,1)*A(2,3) + A(1,3)*A(2,1)
          invA(3,3) = + A(1,1)*A(2,2) - A(1,2)*A(2,1)
          invA = invA/detA


 end subroutine inverse

!====================================================================================================================
 
 pure subroutine average_roe80s(vinl,vinr,av)

  real(wp), intent(in)    :: vinl(nq), vinr(nq)
  real(wp), intent(out)   :: av(4)
  real(wp)                :: invtemp,hl,hr,roe 


  roe = sqrt( vinr(1)/vinl(1) )

  invtemp = 1.0_wp/( sqrt(vinl(1)) + sqrt(vinr(1)) )

!  hl      = gamma0*Ru0*vinl(3)/gm1 + 0.5_wp*vinl(2)*vinl(2)
 ! hr      = gamma0*Ru0*vinr(3)/gm1 + 0.5_wp*vinr(2)*vinr(2)
  hl      = Cp*vinl(3) + 0.5_wp*vinl(2)*vinl(2)
  hr      = Cp*vinr(3) + 0.5_wp*vinr(2)*vinr(2)

  av(1) = ( sqrt(vinl(1))*vinl(2) + sqrt(vinr(1))*vinr(2) )*invtemp    !averaged velocituy
  av(2) = ( sqrt(vinl(1))*hl + sqrt(vinr(1))*hr )*invtemp         !averaged enthalpy
  av(3) = sqrt( ( gm1 )*( av(2)- 0.5_wp*av(1)*av(1) ) ) ! averaged sound speed
  av(4) = roe*vinl(1)
  return
end subroutine average_roe80s

!====================================================================================================================

 pure subroutine average_roe2000s(vl,vr,av)

   real(wp), dimension(nq), intent(in) :: vl,vr
   real(wp), dimension(4), intent(out) :: av           ! 

    real(wp) :: root_l, root_r           
    real(wp) :: tinvav, tinvavinv              ! 

    real(wp) :: z1_ave, z3_ave, a,un                          ! Logarithmic averages of density and temperature

    real(wp) ::  P1, P2, a_hat ,v_hat,h_hat,rho_hat,T                  ! 

    continue

    root_l    = sqrt( 1.0_wp/(Ru0*vl(3)) )  ; root_r  = sqrt( 1.0_wp/(Ru0*vr(3)) )    

    tinvav    = root_l + root_r
    tinvavinv = 1.0_wp/tinvav

    v_hat   = (vl(2)*root_l + vr(2)*root_r)*tinvavinv   ! velocity

    P1      = (vl(1)/root_l + vr(1)/root_r)*tinvavinv             ! pressure, p_1 in (Ismail and Roe)

    z3_ave  = Logarithmic_Average(vl(1)/root_l,vr(1)/root_r)  !z3 log ave          ! Logarithmic average of rho/Sqrt(T)

    z1_ave  = Logarithmic_Average(root_l    ,root_r    )       !z1 log ave       ! Logarithmic average of   1/Sqrt(T)

    rho_hat = 0.5_wp*tinvav*z3_ave                                 ! rho_hat

    P2    = 0.5_wp*( gm1og*P1 + gp1og*z3_ave/z1_ave ) 

    T     = P2/(Ru0*rho_hat)  ! Now  it is P_2_hat
   ! h_hat = gamma0*Ru0*T/gm1 + 0.5_wp*( v_hat*v_hat ) ! total enthalpy
    h_hat = Cp*T + 0.5_wp*( v_hat*v_hat ) ! total enthalpy

    a_hat = sqrt( gamma0*P2/rho_hat )   !a_hat

    av(1) = v_hat
    av(2) = h_hat
    av(3) = a_hat
    av(4) = rho_hat


end subroutine average_roe2000s

!========================================================================================

  pure function sutherland_law(T_in)
    real(wp), intent(in) :: T_in
    real(wp) :: sutherland_law
    real(wp) :: s_c

    ! Sutherland constant divided by the free stream reference temperature in
    ! Rankine
!   s_c = 198.6_wp/(Tref*9.0_wp/5.0_wp)
    s_c = 198.6_wp/(Tref*1.8_wp)

    ! Compute new non-dimensional dynamic viscosity
!   sutherland_law = T_in**(3.0_wp/2.0_wp)*(1.0_wp + s_c)/(T_in + s_c)
    sutherland_law = sqrt(T_in*T_in*T_in)*(1.0_wp + s_c)/(T_in + s_c)

  end function sutherland_law

!=============================================================================================================================
! Subroutines

  subroutine ns1d_setup(ctx) bind(C)
    type(Params), intent(in) :: ctx

    Mach0  = ctx%Mach0
    gamma0 = ctx%gamma0
    Pr0    = ctx%Pr0
    Sfix   = ctx%Sfix
    Ru0    = ctx%Ru0

    gamI   = 1.0/gamma0
    gm1    = gamma0 - 1.0
    gm1I   = 1.0/gm1
    gp1    = gamma0 + 1.0
    gm1og  = gm1/gamma0
    gp1og  = gp1/gamma0
    Cv     = Ru0*gm1I
    Cp     = Ru0*gamma0*gm1I 

  end subroutine ns1d_setup

  pure subroutine ns1d_primitive(ctx,U,V) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(out) :: V(dof)
    call conserved_to_primitive(U,V)
  end subroutine ns1d_primitive

  pure subroutine ns1d_conservative(ctx,V,U) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: V(dof)
    real(kind=rk), intent(out) :: U(dof)
    call primitive_to_conserved(V,U)
  end subroutine ns1d_conservative

  pure subroutine ns1d_entropy(ctx,U,W) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(out) :: W(dof)
    call conserved_to_entropy(U,W)
  end subroutine ns1d_entropy

  pure subroutine ns1d_primary(ctx,W,U) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: W(dof)
    real(kind=rk), intent(out) :: U(dof)
    real(kind=rk)  :: V(dof)
    call entropy_to_primitive(W,V)
    call primitive_to_conserved(V,U)
  end subroutine ns1d_primary

  pure subroutine ns1d_dWdV(ctx,U,W_V) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(out) :: W_V(dof,dof)
    real(kind=rk)  :: V(dof), mat(dof,dof)
    call conserved_to_primitive(U,V)
    mat = dWdV(V)
    W_V = transpose(mat)
  end subroutine ns1d_dWdV

  pure subroutine ns1d_dVdW(ctx,U,V_W) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(out) :: V_W(dof,dof)
    real(kind=rk)  :: V(dof), mat(dof,dof)
    call conserved_to_primitive(U,V)
    mat = dVdW(V)
    V_W = transpose(mat)
  end subroutine ns1d_dVdW

  pure subroutine ns1d_grad_w2u(ctx,U,dWdX,dUdX) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: dWdX(dim,dof)
    real(kind=rk), intent(out) :: dUdX(dim,dof)
    real(kind=rk)  :: V(dof), mat(dof,dof)
    call conserved_to_primitive(U,V)
    mat = dUdW(V)
    dUdX = matmul(dWdX,transpose(mat))
  end subroutine ns1d_grad_w2u

  pure subroutine ns1d_grad_u2w(ctx,U,dUdX,dWdX) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: dUdX(dim,dof)
    real(kind=rk), intent(out) :: dWdX(dim,dof)
    real(kind=rk)  :: V(dof), mat(dof,dof)
    call conserved_to_primitive(U,V)
    mat = dWdU(V)
    dWdX = matmul(dUdX,transpose(mat))
  end subroutine ns1d_grad_u2w

  pure subroutine ns1d_grad_w2v(ctx,U,dWdX,dVdX) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: dWdX(dim,dof)
    real(kind=rk), intent(out) :: dVdX(dim,dof)
    real(kind=rk)  :: V(dof), mat(dof,dof)
    call conserved_to_primitive(U,V)
    mat = dVdW(V)
    dVdX = matmul(dWdX,transpose(mat))
  end subroutine ns1d_grad_w2v

  pure subroutine ns1d_grad_v2w(ctx,U,dVdX,dWdX) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: dVdX(dim,dof)
    real(kind=rk), intent(out) :: dWdX(dim,dof)
    real(kind=rk)  :: V(dof), mat(dof,dof)
    call conserved_to_primitive(U,V)
    mat = dWdV(V)
    dWdX = matmul(dVdX,transpose(mat))
  end subroutine ns1d_grad_v2w


  pure subroutine ns1d_iflux(ctx,U,F) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(out) :: F(dim,dof)
    real(kind=rk)  :: V(dof),n(dim)
    real(kind=rk)  :: Ft(dof,dim)
    integer   :: i
    call conserved_to_primitive(U,V)
    do i=1,dim 
       n(:) = 0.0_wp
       n(i) = 1.0_wp
       Ft(:,i) = NormalFlux(V,n)
    end do
    F = transpose(Ft)
  end subroutine ns1d_iflux


  pure subroutine ns1d_iflux1n(ctx,U,Sn,Fn) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: Sn(dim)
    real(kind=rk), intent(out) :: Fn(dof)
    real(kind=rk)  :: V(dof)
    call conserved_to_primitive(U,V)
    Fn = NormalFlux(V,Sn)
  end subroutine ns1d_iflux1n

  pure subroutine ns1d_iflux2n(ctx,Um,Up,Sn,Fn) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: Um(dof)
    real(kind=rk), intent(in)  :: Up(dof)
    real(kind=rk), intent(in)  :: Sn(dim)
    real(kind=rk), intent(out) :: Fn(dof)
    real(kind=rk)  :: Vm(dof),Vp(dof)
    call conserved_to_primitive(Um,Vm)
    call conserved_to_primitive(Up,Vp)
    Fn = EntropyConsistentFlux(Vm,Vp,Sn)
    !Fn = Entropy_KE_Consistent_Flux(Vm,Vp,Sn)
  end subroutine ns1d_iflux2n

  pure subroutine ns1d_upwind(ctx,Um,Up,Sn,upw) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: Um(dof)
    real(kind=rk), intent(in)  :: Up(dof)
    real(kind=rk), intent(in)  :: Sn(dim)
    real(kind=rk), intent(out) :: upw(dof)
    real(kind=rk)  :: Vm(dof),Vp(dof)
    real(kind=rk)  :: Wm(dof),Wp(dof),dW(dof),dU(dof)
    real(kind=rk)  :: ev(dof),evabs(dof)
    real(kind=rk)  :: r_mat(dof,dof),vec(dof),rmatinv(dof,dof)
    call conserved_to_primitive(Um,Vm)
    call conserved_to_entropy(Um,Wm)
    call conserved_to_primitive(Up,Vp)
    call conserved_to_entropy(Up,Wp)

    !real(kind=rk)  :: Vav(dof)
    !call roeavg(Vm,Vp,Vav)
    
    call CharacteristicDecompEU(Vm,Vp,r_mat,ev,Sn)
    !call CharacteristicDecompEURoe(Vm,Vp,r_mat,ev,Sn)
    !evabs = sqrt(ev*ev + Sfix*maxval(abs(ev)))
    evabs = abs(ev)
    evabs = sqrt(ev*ev + Sfix*maxval(evabs))
    dW  = Wm - Wp
   ! dU  = Um - Up
    vec = evabs * matmul(transpose(r_mat),dW)
  !  call inverse(r_mat,rmatinv)
  !  vec = evabs * matmul(rmatinv,dU)

    upw = half * matmul(r_mat,vec)
  end subroutine ns1d_upwind

    pure subroutine ns1d_vflux(ctx,U,G,F) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: G(dim,dof)
    real(kind=rk), intent(out) :: F(dim,dof)
    real(kind=rk)  :: V(dof)
    real(kind=rk)  :: Gt(dof,dim)
    real(kind=rk)  :: Ft(dof,dim)
    call conserved_to_primitive(U,V)
    Gt = transpose(G)
    Ft =  ViscousFlux(V,Gt)
    F = transpose(Ft)
  end subroutine ns1d_vflux


  pure subroutine ns1d_vfluxn(ctx,U,G,Sn,Fn) bind(C)
    type(c_ptr),   intent(in), value :: ctx
    real(kind=rk), intent(in)  :: U(dof)
    real(kind=rk), intent(in)  :: G(dim,dof)
    real(kind=rk), intent(in)  :: Sn(dim)
    real(kind=rk), intent(out) :: Fn(dof)
    real(kind=rk)  :: V(dof)
    real(kind=rk)  :: Gt(dof,dim)
    real(kind=rk)  :: Ft(dof,dim)
    call conserved_to_primitive(U,V)
    Gt = transpose(G)
    Ft =  ViscousFlux(V,Gt)
    Fn = matmul(Ft,Sn)
  end subroutine ns1d_vfluxn

  ! ----------------------------------------------------------------------

end module navierstokes1d
